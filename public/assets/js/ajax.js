var Ajax = function () {
    return {

        onBeforeSend: function (options) {

        },

        onComplete: function (options) {

        },

        onSuccess: function (options) {

        },

        onError: function (options) {

        },

        reset: function () {
            Ajax.onBeforeSend = function (ret) {};
            Ajax.onComplete = function (ret) {};
            Ajax.onSuccess = function (ret) {};
            Ajax.onError = function (ret) {};
        }
    }
}();