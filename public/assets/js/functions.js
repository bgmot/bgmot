var Functions = function () {
    return {
        escapeRegExp: function (string) {
            return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        },

        replaceAll: function (string, find, replace) {
            return string.replace(new RegExp(Functions.escapeRegExp(find), 'g'), replace);
        },

        ucfirst: function (str) {
            str += '';
            var f = str.charAt(0).toUpperCase();
            return f + str.substr(1).toLowerCase();
        },
    }
}();