var Common = function () {
    return {
        initAjax: function () {
            $.ajaxSetup({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "json",
                cache: false,
            });
        },
        ajaxErrors: function () {
            $(document).ajaxError(function (event, request, settings) {
                if (request.status == 404) {
                    Common.showNotification("Error", "404 Not Found Eh ?!?!");
                }

                if (request.status == 401) {
                    Common.showNotification("Error", "You dont have access to the required resource. Please contact administrator.");
                }

                if (request.status == 419) {
                    Common.showNotification("Error", "Seems you have been logged out. Please re-login");
                    setTimeout(function () {
                        /*App.blockUI({
                            target: 'body',
                            animate: true
                        });*/

                        window.location = '/auth/logout';

                    }, 1500);
                }
            });
        },
        _ajaxRequest: function (url, options, callbacks, dataType) {
            dataType = dataType || 'json';
            var verticalPrefix = '';

            if (typeof $('meta[name="vertical"]').attr('content') != 'undefined') {
                if ($('meta[name="vertical"]').attr('content') != '') {
                    verticalPrefix = "/" + $('meta[name="vertical"]').attr('content');
                }
            }

            $.ajax({
                url: verticalPrefix + url,
                data: options,
                dataType: dataType,
                beforeSend: function () {
                    if (typeof callbacks.onBeforeSend == "function") {
                        callbacks.onBeforeSend();
                    }
                    if (options.hasOwnProperty('selector')) {
                        /*App.blockUI({
                            target: options.selector,
                            animate: true
                        });*/
                    }

                    callbacks.onBeforeSend = function () {
                    };
                },
                complete: function () {
                    if (options.hasOwnProperty('selector')) {
                        //App.unblockUI(options.selector);
                    }

                    if (typeof callbacks.onComplete == "function") {
                        callbacks.onComplete();
                    }
                    callbacks.onComplete = function () {
                    };
                },
                success: function (data) {
                    if (options.hasOwnProperty('selector')) {
                        //App.unblockUI(options.selector);
                    }

                    if (typeof callbacks.onSuccess == "function") {
                        console.log('haha');
                        callbacks.onSuccess(data);
                    }

                    callbacks.onSuccess = function () {
                    };
                },
                error: function (data) {
                    if (options.hasOwnProperty('selector')) {
                        //App.unblockUI(options.selector);
                    }

                    if (typeof callbacks.onError == "function") {
                        callbacks.onError(data);
                    }
                    callbacks.onError = function () {
                    };
                }
            });
        },

        showNotification: function (title, message) {
            var $notificationModal = $('#notification-modal');

            $('.notification-modal-body').html(message);
            if (title != '') {
                $('.notification-modal-title').html(title);
            }

            $notificationModal.modal('show');

            setTimeout(function () {
                $notificationModal.modal('hide');
            }, 2500);
        },

        modals: function (modalName) {
            var $modals = {
                register: $('#register-modal'),
                login: $('#login-modal'),
            };

            return $modals[modalName];
        },

    }
}();

var $body = $('body');
var $windowLocation = window.location;

$(document).ready(function () {
    Common.initAjax();
    Common.ajaxErrors();


});