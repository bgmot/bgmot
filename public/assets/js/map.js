var Map = function () {
    return {
        selectArea: function (options, callbacks) {
            Common._ajaxRequest('/ajax/load-area', options, callbacks);
        },

        selectCity: function (options, callbacks) {
            Common._ajaxRequest('/ajax/load-city', options, callbacks);
        }
    }
}();

$(document).ready(function () {
    var ajaxFunctions = Ajax;

    $body.on('click', '.area-selector', function (e) {
        e.preventDefault();
        var options = {
            area_id: $(this).attr('data-area-id'),
            selector: '.map-selector'
        };

        ajaxFunctions.onSuccess = function (ret) {
            if (ret.error.code == 0) {

            } else {
                Common.showNotification("Error", ret.error.description);
            }
        };

        Map.selectArea(options, ajaxFunctions);
    });

    $body.on('click', '.city-selector', function (e) {
        e.preventDefault();
        var options = {
            area_id: $(this).attr('data-area-id'),
            city_id: $(this).attr('data-city-id'),
            selector: '.map-selector'
        };

        ajaxFunctions.onSuccess = function (ret) {
            if (ret.error.code == 0) {

            } else {
                Common.showNotification("Error", ret.error.description);
            }
        };

        Map.selectCity(options, ajaxFunctions);
    });
});