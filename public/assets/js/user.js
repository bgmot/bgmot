var User = function () {
    return {
        clearFeedbacks: function () {
            $('div.invalid-feedback').each(function (a, b) {
                $(b).html('');
            });
        },

        loadRegisterForm: function (options, callbacks) {
            Common._ajaxRequest('/auth/ajax/load-register-form', options, callbacks);
        },

        registerUser: function (options, callbacks) {
            Common._ajaxRequest('/auth/ajax/register-user', options, callbacks);
        },

        checkLoginInfo: function (options, callbacks) {
            Common._ajaxRequest('/auth/ajax/check-login-info', options, callbacks);
        },
    }
}();

$(document).ready(function () {
    var ajaxFunctions = Ajax;

    $body.on('click', '.login-modal', function (e) {
        e.preventDefault();
        Common.modals('login').modal('show');
    });


    $body.on('click', '.register-modal', function (e) {
        e.preventDefault();
        Common.modals('register').modal('show');
    });

    $body.on('click', '.change-user-type', function (e) {
        e.preventDefault();
        var $thisVal = $(this).val();
        var options = {
            user_type: $thisVal,
            selector: '#register-modal-body'
        };

        ajaxFunctions.onSuccess = function (ret) {
            User.clearFeedbacks();

            if (ret.error.code == 0) {
                $('.register-user-type').val($thisVal);
                $('.user-type-form').html(ret.data.html);
                $('.register-now').show();
            } else {
                Common.showNotification("Error", ret.error.description);
            }
        };

        User.loadRegisterForm(options, ajaxFunctions);
    });


    $body.on('click', 'button.login-button', function (e) {
        e.preventDefault();

        var options = {
            email: $('input.login-email').val(),
            password: $('input.login-password').val(),
            selector: '#login-modal-body'
        };
        ajaxFunctions.onSuccess = function (ret) {
            if (ret.error.code == 0) {
                if (ret.data.message == 'Success') {
                    window.location = window.location;
                }
            } else {

                $html = '';

                $('.login-modal-body .errors').html('<div class="alert alert-danger">\n' +
                    '                            <strong>Whoops!</strong> There were some problems with your input.<br><br>\n' +
                    '                            <ul>\n' +
                    '<li>' + ret.error.description + '</li>\n' +
                    '                            </ul>\n' +
                    '                        </div>');

                Common.showNotification("Error", ret.error.description);
            }
        };

        User.checkLoginInfo(options, ajaxFunctions);
    });

    $body.on('click', 'button.register-button', function (e) {
        e.preventDefault();

        var options = {
            data: $('.register-modal-body :input').serialize(),
            selector: '#register-modal-body'
        };

        ajaxFunctions.onSuccess = function (ret) {
            User.clearFeedbacks();
            if (ret.error.code == 0) {

            } else {
                $.each(ret.data.messages, function (i, j) {
                    $('div.invalid-feedback-' + i).html('');
                    $('div.invalid-feedback-' + i).hide();
                    $.each(j, function (x, y) {
                        $('div.invalid-feedback-' + i).append('<p class="small">' + y + '</p>');
                    });

                    $('div.invalid-feedback-' + i).show();
                });
            }
        };

        User.registerUser(options, ajaxFunctions);
    });
});