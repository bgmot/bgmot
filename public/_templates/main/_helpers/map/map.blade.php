<section id="mainmap" class="padded">
    <div class="wrapper wrapper-padded">
        <div class="map-wrapper">
            <div class="map-holder" id="countryMap">
                {!! \Helpers\Map::generate() !!}
            </div>
        </div>
    </div>
</section>