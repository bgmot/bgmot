<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header register-modal-header">
                <h5 class="modal-title">{{ __('translate.register.title') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body register-modal-body">
                {{ Form::hidden('user_type', null, ['class' => 'register-user-type']) }}
                <div class="row text-center">
                    <div class="col-md-12 text-center">
                        @foreach (\Modules\User\Entities\UserType::where('is_administrator', 0)->get() as $userType)
                            <div class="form-group">
                                {!! Form::label('user_type', __('translate.register.select.' . strtolower($userType->name))) !!}
                                {!! Form::radio('user_type', $userType->id, null, ['class' => 'form-control change-user-type']) !!}
                            </div>
                        @endforeach
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        {!! Form::label('firstname', __('translate.register.form.firstname')) !!}
                        {!! Form::text('firstname', null, ['class' => 'form-control register-firstname', 'id' => 'firstname', 'required']) !!}
                        <div class="invalid-feedback invalid-feedback-firstname"></div>
                    </div>
                    <div class="col-md-6 mb-3">
                        {!! Form::label('lastname', __('translate.register.form.lastname')) !!}
                        {!! Form::text('lastname', null, ['class' => 'form-control register-lastname', 'id' => 'lastname', 'required']) !!}
                        <div class="invalid-feedback invalid-feedback-lastname"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        {!! Form::label('email', __('translate.register.form.email')) !!}
                        {!! Form::email('email', null, ['class' => 'form-control register-email', 'id' => 'email', 'required']) !!}
                        <div class="invalid-feedback invalid-feedback-email"></div>
                    </div>
                    <div class="col-md-6 mb-3">
                        {!! Form::label('phone', __('translate.register.form.phone')) !!}
                        {!! Form::text('phone', null, ['class' => 'form-control register-phone', 'id' => 'phone', 'required']) !!}
                        <div class="invalid-feedback invalid-feedback-phone"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <div class="form-group">
                            {!! Form::label('area_id', __('translate.register.form.area')) !!}
                            {!! Form::text('area_id', null, ['class' => 'form-control register-area_id', 'required']) !!}
                            <div class="invalid-feedback invalid-feedback-area_id"></div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <div class="form-group">
                            {!! Form::label('city_id', __('translate.register.form.city')) !!}
                            {!! Form::text('city_id', null, ['class' => 'form-control register-city_id', 'required']) !!}
                        </div>
                        <div class="invalid-feedback invalid-feedback-city_id"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        {!! Form::label('password', __('translate.register.form.password')) !!}
                        {!! Form::password('password', ['class' => 'form-control register-password', 'id' => 'password', 'required']) !!}
                        <div class="invalid-feedback invalid-feedback-password"></div>
                    </div>
                    <div class="col-md-6 mb-3">
                        {!! Form::label('password_confirmation', __('translate.register.form.password_confirmation')) !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control register-password_confirmation', 'id' => 'password_confirmation', 'required']) !!}
                        <div class="invalid-feedback invalid-feedback-password_confirmation"></div>
                    </div>
                </div>
                <div class="user-type-form">

                </div>
                <hr class="mb-4">
                <div class="register-now" style="display:none;">
                    <button class="btn btn-primary btn-lg btn-block register-button"
                            type="button">{{ __('translate.register.form.button') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>