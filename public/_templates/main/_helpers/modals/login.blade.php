<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header login-modal-header">
                <h5 class="modal-title">{{ __('translate.login.title') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </div>
            <div class="modal-body login-modal-body">
                <form class="form-login" method="POST" action="{{ url('/auth/login') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="errors">

                    </div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <label for="inputEmail" class="sr-only">{{ __('translate.login.form.email') }}</label>
                    <input type="email" id="inputEmail" class="form-control login-email" placeholder="Email address"
                           name="email"
                           required value="{{ old('email') }}" autofocus>
                    <label for="inputPassword" class="sr-only">{{ __('translate.login.form.password') }}</label>
                    <input type="password" id="inputPassword" class="form-control login-password" name="password"
                           placeholder="Password" required>
                    <div class="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="remember"
                                   value="1">{{ __('translate.login.form.remember_me') }}
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block login-button"
                            type="button">{{ __('translate.login.form.button') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>