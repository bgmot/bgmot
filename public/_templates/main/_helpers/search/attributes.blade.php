<section class="padded" id="secondStep">
    <div class="wrapper wrapper-padded">
        <form action="" enctype="multipart/form-data" method="get">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="region">Област</label>
                        <input class="form-control" value="" type="text" name="region" placeholder="Област"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="region">Населено място</label>
                        <input class="form-control" value="" type="text" name="subregion" placeholder="Населено място"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="region">Квартали</label>
                        <select class="form-control" name="neighbor-list" id="neighbor-list" size="5">
                            <option value="0"></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-1 d-flex align-items-center justify-content-end">
                    <button class="btn btn-outline">
                        <i class="far fa-angle-left"></i>
                    </button>
                </div>
                <div class="col-md-1 d-flex align-items-center justify-content-start">
                    <button class="btn btn-outline">
                        <i class="far fa-angle-right"></i>
                    </button>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="region">Избрани квартали</label>
                        <select class="form-control" multiple name="neighbor-list" id="neighbor-list" size="5">
                            <option value="0"></option>
                        </select>
                    </div>
                </div>
            </div>

        </form>
    </div>
</section>