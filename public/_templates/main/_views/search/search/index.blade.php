@extends('Main::layout')

@section('content')
    @includeIf('Helpers::map.map')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
            {{ Form::open(['method' => 'POST', 'route' => 'search.search', 'id' => 'validate-form', 'enctype' => 'multipart/form-data']) }}
            @include('Search::search.form', ['submitButtonText' => 'Search'])
            {{ Form::close() }}
        </div>
    </div>
@endsection