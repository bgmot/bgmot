<div class="row">
    <div class="col-md-12">
        <hr class="mb-4">
    </div>

    <div class="col-md-6 mb-3">
        <div class="form-group">
            {!! Form::label('ad_type_id', __('translate.register.form.ad_type')) !!}
            {!! Form::select('ad_type_id', $adsType->pluck('name', 'id')->toArray(), null, ['class' => 'form-control register-ad_type_id', 'required']) !!}
            <div class="invalid-feedback invalid-feedback-ad_type_id"></div>
        </div>
    </div>

    <div class="col-md-6 mb-3">
        <div class="form-group">
            {!! Form::label('user_sub_id', __('translate.register.form.user_sub_type')) !!}
            {!! Form::select('user_sub_id', [
                1   =>  __('translate.register.form.user_sub.seek'),
                2   =>  __('translate.register.form.user_sub.owner'),
            ], null, ['class' => 'form-control register-user_sub_id', 'required']) !!}
            <div class="invalid-feedback invalid-feedback-user_sub_id"></div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <hr class="mb-4">
    </div>

    <div class="col-md-12">
        <h4>Данни за предлагания от вас имот:</h4>
        <hr class="mb-4">

        @foreach($attributeTypes as $attributeType)
            <div class="row">
                <div class="col-md-12">
                    <h4>{{ __('translate.register.form.' . strtolower($attributeType->name)) }}</h4>
                    <hr class="mb-4">
                </div>
                @foreach ($attributeType->attribute()->where('is_enabled', 1)->get() as $attribute)
                    <div class="col-md-2">
                        <div class="form-group">
                            @php
                                $attributeClass = $attribute->class;
                                $attributeLabel = $attribute->label;
                                $attributeId = $attribute->name.'_'.$attribute->id;
                                $attributeName = $attribute->name;
                                $attributeForm = 'attribute['.$attribute->id.']';
                            @endphp
                            @if ($attribute->type == 'checkbox' || $attribute->type == 'radio')
                                {!! Form::{$attribute->type}($attributeForm, ((!is_null($attribute->value) && $attribute->type == 'select') ? json_decode($attribute->value) : $attribute->value), false, (['class' => 'form-control register-' . strtolower($attributeName) . ' ' . $attributeClass, 'id' => $attributeId]) ) !!}
                                {!! Form::label($attributeId, $attributeName) !!}
                            @elseif ($attribute->type == 'select')
                                {!! Form::{$attribute->type}($attributeForm, ((!is_null($attribute->value) && $attribute->type == 'select') ? json_decode($attribute->value) : $attribute->value), (['class' => 'form-control register-' . strtolower($attributeName) . ' ' . $attributeClass, 'id' => $attributeId]) ) !!}
                                {!! Form::label($attributeId, $attributeName) !!}
                            @else
                                {!! Form::{$attribute->type}($attributeForm, $attribute->value, (['class' => 'form-control register-' . strtolower($attributeName) . ' ' . $attributeClass, 'id' => $attributeId]) ) !!}
                                {!! Form::label($attributeId, $attributeName) !!}
                            @endif
                        </div>
                    </div>

                @endforeach
            </div>
        @endforeach
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr class="mb-4">
    </div>

    <div class="col-md-6 mb-3">
        <div class="form-group">
            {!! Form::label('area_id', __('translate.register.form.area')) !!}
            {!! Form::text('area_id', null, ['class' => 'form-control register-area_id', 'required']) !!}
            <div class="invalid-feedback invalid-feedback-area_id"></div>
        </div>
    </div>
    <div class="col-md-6 mb-3">
        <div class="form-group">
            {!! Form::label('city_id', __('translate.register.form.city')) !!}
            {!! Form::text('city_id', null, ['class' => 'form-control register-city_id', 'required']) !!}
        </div>
        <div class="invalid-feedback invalid-feedback-city_id"></div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('neighbor_id', __('translate.register.form.neighbor')) !!}
            {!! Form::text('neighbor_id', null, ['class' => 'form-control register-neighbor_id', 'required']) !!}
        </div>
        <div class="invalid-feedback invalid-feedback-city_id"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr class="mb-4">
    </div>

    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('price', __('translate.register.form.price')) !!}
            {!! Form::text('price', null, ['class' => 'form-control register-price', 'required']) !!}
        </div>
        <div class="invalid-feedback invalid-feedback-price"></div>
    </div>
    <div class="col-md-4">
        @foreach ($currencyAttributeTypes as $currencyAttributeType)
            @foreach ($currencyAttributeType->attribute()->where('is_enabled', 1)->get() as $attribute)
                <div class="form-group">
                    {!! Form::label('currency', $attribute->name) !!}
                    {!! Form::radio('currency', $attribute->id, null, ['class' => 'form-control']) !!}
                </div>
            @endforeach
        @endforeach
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <hr class="mb-4">
    </div>

    <div class="col-md-6 mb-3">
        <div class="form-group">
            {!! Form::label('floor_from', __('translate.register.form.floor')) !!}
            {!! Form::select('floor_from', range(0,30), null, ['class' => 'form-control register-floor_from', 'required']) !!}
            <div class="invalid-feedback invalid-feedback-floor_from"></div>
        </div>
    </div>
    <div class="col-md-6 mb-3">
        <div class="form-group">
            {!! Form::label('floor_to', __('translate.register.form.floor')) !!}
            {!! Form::select('floor_to', range(0,30), null, ['class' => 'form-control register-floor_to', 'required']) !!}
        </div>
        <div class="invalid-feedback invalid-feedback-floor_to"></div>
    </div>
</div>