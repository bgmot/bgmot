<div class="row">
    <div class="col-md-12">
        <hr class="mb-4">
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('name', __('translate.register.form.agency.name')) !!}
                    {!! Form::text('name', null, ['class' => 'form-control register-name', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-name"></div>
                </div>
                <div class="form-group">
                    {!! Form::label('address', __('translate.register.form.agency.address')) !!}
                    {!! Form::text('address', null, ['class' => 'form-control register-address', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-address"></div>
                </div>
                <div class="form-group">
                    {!! Form::label('id_number', __('translate.register.form.agency.id_number')) !!}
                    {!! Form::text('id_number', null, ['class' => 'form-control register-id_number', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-id_number"></div>
                </div>
                <div class="form-group">
                    {!! Form::label('tax_number', __('translate.register.form.agency.tax_number')) !!}
                    {!! Form::text('tax_number', null, ['class' => 'form-control register-tax_number', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-tax_number"></div>
                </div>

            </div>

            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('responsible_person', __('translate.register.form.agency.responsible_person')) !!}
                    {!! Form::text('responsible_person', null, ['class' => 'form-control register-responsible_person', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-responsible_person"></div>
                </div>
                <div class="form-group">
                    {!! Form::label('contact_person', __('translate.register.form.agency.contact_person')) !!}
                    {!! Form::text('contact_person', null, ['class' => 'form-control register-contact_person', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-contact_person"></div>
                </div>
                <div class="form-group">
                    {!! Form::label('phone', __('translate.register.form.agency.phone')) !!}
                    {!! Form::text('phone', null, ['class' => 'form-control register-phone', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-phone"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <hr class="mb-4">
    </div>

    <div class="col-md-12">
        <h4>{{ __('translate.register.form.agency.public_data') }}</h4>
        <hr class="mb-4">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('public_name', __('translate.register.form.agency.public_name')) !!}
                    {!! Form::text('public_name', null, ['class' => 'form-control register-public_name', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-public_name"></div>
                </div>
                <div class="form-group">
                    {!! Form::label('public_address', __('translate.register.form.agency.public_address')) !!}
                    {!! Form::text('public_address', null, ['class' => 'form-control register-public_address', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-public_address"></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('public_area_id', __('translate.register.form.agency.public_area')) !!}
                            {!! Form::text('public_area_id', null, ['class' => 'form-control register-area_id', 'required']) !!}
                            <div class="invalid-feedback invalid-feedback-public_area_id"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('public_city_id', __('translate.register.form.agency.public_city')) !!}
                            {!! Form::text('public_city_id', null, ['class' => 'form-control register-city_id', 'required']) !!}
                            <div class="invalid-feedback invalid-feedback-public_city_id"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('public_phone', __('translate.register.form.agency.public_phone')) !!}
                    {!! Form::text('public_phone', null, ['class' => 'form-control register-public_phone', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-public_phone"></div>
                </div>
                <div class="form-group">
                    {!! Form::label('public_web', __('translate.register.form.agency.public_web')) !!}
                    {!! Form::text('public_web', null, ['class' => 'form-control register-public_web', 'required']) !!}
                    <div class="invalid-feedback invalid-feedback-public_web"></div>
                </div>

            </div>
        </div>
    </div>
</div>