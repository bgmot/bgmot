//Hamburger

$('.menu-bars').click(function(e){
    $(this).find('span').toggleClass('active'); // animate bars to X
    $('nav.navbar').toggleClass('show');

    if($('header').hasClass('fixed')) {
        $('nav.navbar').toggleClass('show'); // If top toolbar is fixed, initiate fixed nav
    }
});

//DropDown

$(document).on('click', function(e) {
    if ($(e.target).is(".dropdown.active") === false) {
        $(".dropdown.active").removeClass("active");
    }
});

$('.dropdown-trigger').on('click',function(e){
    e.stopPropagation();
    $('.dropdown.active').removeClass('active');
    $(this).parent().find('.dropdown').toggleClass('active');
});


$(document).ready(function(){

    //Tooltip

    $('.tooltiped').each(function(){
        $(this).tooltipster({
            anchor: 'bottom-right',
            content: $(this).data('title'),
            offset: [50, 0],
            plugins: ['follower'],
            theme: 'tooltipster-borderless',
        });
    });
})