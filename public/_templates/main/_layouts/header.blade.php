<meta charset="utf-8"/>
<title>{{  $appConfig->get('settings', 'html.title') }} | User Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="{{ $appConfig->get('settings', 'html.title') }}" name="description"/>
<meta content="{{ $appConfig->get('settings', 'author_name') }}" name="author"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>BGMot</title>

<!-- Custom styles for this template -->
<link href="/_templates/main/_assets/css/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />


<style type="text/css">
    .fon{fill:#FFFFFF;}
    .st0{fill:#7E459A;}
    .st1{fill:#FFFFFE;}
</style>