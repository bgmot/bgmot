<!-- header -->
<header class="header top-toolbar" role="banner">
    <div class="wrapper">
        {{-- Да не заборам да коментирам --}}
        <div class="nav-left">
            <div class="menu-bars toggle" data-target="nav-wrapper">
                <span class=""></span>
            </div>
            <div class="logo-holder">
                <!-- logo -->
                <a href="/" class="logo">
                    <span>BgMot</span>
                </a>
                <!-- /logo -->
            </div>
            <nav class="navbar">
                <ul class="menunav">
                    <li><a href="/search">Търсене</a></li>
                    <li><a href="{{ route('agency.agencycontroller.index') }}">Агенции</a></li>
                    <li><a href="{{ route('agency.brokercontroller.index') }}">Брокери</a></li>
                    <li><a href="/property-looking">Търсещи имот</a></li>
                    <li><a href="/property-offering">Предлагащи имот</a></li>
                </ul>
            </nav>
        </div>

        <div class="logo-holder">
            <!-- logo -->
            <a href="/" class="logo">
                <span>BgMot</span>
            </a>
            <!-- /logo -->
        </div>

        <div class="pull-right nav-right">

            @if (\Auth::check())
                <div class="header-profile icon-group">
                    <a href="#">
                            <span class="header-profile-thumb">
                                {{--
                                 <img src="PATEKA DO THUMB NA PROFIL" />
                                --}}
                            </span>
                        <span class="header-profile-name">
                                {{ $appConfig->getUser()->detail->firstname }}
                            </span>
                    </a>
                </div>
                <div class="icon-group">
                    <div class="btn-holder">
                           <span class="icon-holder dropdown-trigger">
                               @if ($appConfig->getUser()->has_request)
                                   <i class="fas fa-user"></i>
                               @else
                                   <i class="fal fa-user"></i>
                               @endif
                               <div class="dropdown">
                                    <ul>
                                        <li><a href="#">Покана за приятел</a></li>
                                    </ul>
                                </div>
                            </span>
                    </div>
                    <div class="btn-holder">
                           <span class="icon-holder dropdown-trigger">
                               @if ($appConfig->getUser()->has_message)
                                   <i class="fas fa-envelope"></i>
                               @else
                                   <i class="fal fa-envelope"></i>
                               @endif
                               <div class="dropdown">
                                    <ul>
                                        <li><a href="#">Съобщение</a></li>
                                    </ul>
                                </div>
                            </span>
                    </div>
                    <div class="btn-holder">
                           <span class="icon-holder dropdown-trigger">
                               @if ($appConfig->getUser()->has_notification)
                                   <i class="fas fa-bell"></i>
                               @else
                                   <i class="fal fa-bell"></i>
                               @endif
                               <div class="dropdown">
                                    <ul>
                                        <li><a href="#">Нотификейшан</a></li>
                                    </ul>
                                </div>
                           </span>
                    </div>
                </div>
                <div class="btn-holder">
                        <span class="icon-holder dropdown-trigger">
                            <i class="fas fa-chevron-down"></i>
                        </span>
                    <div class="dropdown">
                        @php
                            $language = 'English';
                            $locale = 'en';
                            if (\App::getLocale() == 'en') {
                                $language = 'Български';
                                $locale = 'bg';
                            }
                        @endphp
                        <ul>
                            <li><a href="/change-lang/{{ $locale }}">{{ $language }}</a></li>
                            <li><a href="/user/settings">Настройки</a></li>
                            <li><a href="/auth/logout">Изход</a></li>
                        </ul>
                    </div>
                </div>
            @else
                <a href="#" title="Log in" class="login-modal">
                    <i class="fal fa-sign-in-alt"></i> <span class="login-modal">Вход</span>
                </a>
                <a href="#" title="Register" class="register-modal">
                    <i class="fal fa-user-plus"></i> <span class="register-modal">Регистрация</span>
                </a>
            @endif
        </div>

    </div>
</header>
<!-- /header -->