<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    @includeIf('Main::header')
</head>

<body>
@includeIf('Main::navigation')

        @yield('content')

@includeIf('Helpers::modals.notification')

@if (!\Auth::check())
    @includeIf('Helpers::modals.register')
    @includeIf('Helpers::modals.login')
@endif


@includeIf('Main::footer')

</body>

</html>