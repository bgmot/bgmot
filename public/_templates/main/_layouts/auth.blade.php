<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    @includeIf('Main::header')
</head>

<body class="text-center">


@yield('content')

@includeIf('Main::footer')

</body>

</html>