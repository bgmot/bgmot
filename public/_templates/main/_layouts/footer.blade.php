<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="/_templates/main/_assets/js/bootstrap.js"></script>

<script src="/assets/js/ajax.js" type="text/javascript"></script>
<script src="/assets/js/global.js" type="text/javascript"></script>
<script src="/assets/js/functions.js" type="text/javascript"></script>
<script src="/assets/js/app.js" type="text/javascript"></script>
<script src="/assets/js/user.js" type="text/javascript"></script>


<script src="/assets/js/map.js" type="text/javascript"></script>

<!-- include any custom template JS -->
<script src="/_templates/main/_assets/js/essentials.js"></script>

<script src="/_templates/main/_assets/js/tooltipster.bundle.min.js"></script>