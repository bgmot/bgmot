<?php

return [
	'regex'          => '~',
	'regex_modifier' => 'i',
	'variable_start' => '[[',
	'variable_end'   => ']]',
];