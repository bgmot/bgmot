<?php

return [
	'engine'   => [
		'variable'       => '{engine}',
		'allowed-engine' => [
			'app',
			'customer',
		],
		'default-engine' => 'app',
	],
	'crypt'    => [
		'key'         => 's[xNKgx*}1#08iS^m]!GzhT1s9L{p%Tb',
		'method'      => 'AES-256-CBC',
		'hashAlgo'    => 'sha512',
		'ivSeparator' => '::',
	],
	'customer' => [
		'variable'         => '{customer}',
		'default-customer' => 'app',
	],

	'default-avatar' => [
		'user'     => '/avatars/user/default-user.jpeg',
		'customer' => '/avatars/customer/default-customer.jpeg',
	],
	'global'         => [
		'backend-template' => '/_templates/avalon',
		'salt'             => '',
		'is_private'       => 1,
	],
	'cache'          => [
		'routes' => 15, // In minutes
	],
	'filemanager'    => [
		'database' => [
			'extensions' => [
				'csv',
				'txt',
				//'xls',
				//'xlsx',

			],
		],
	],

];