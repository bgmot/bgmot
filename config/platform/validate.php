<?php

return [
	'default' => [
		'rules'    => [
			'firstname' => 'required|string|max:255',
			'lastname'  => 'required|string|max:255',
			'email'     => 'required|string|email|max:255|unique:user',
			'phone'     => 'required|digits:10',
			'password'  => 'required|string|min:6|confirmed',
		],
		'messages' => [
			'firstname.required' => 'FirstName is required',
			'lastname.required'  => 'LastName is required',
			'email.required'     => 'Email is required',
			'email.unique'       => 'Email is already in use',
			'phone.required'     => 'Phone is required',
			'phone.digits'       => 'Phone must be only digits',
			'password.required'  => 'Password is required',
		],
	],

	'user' => [
		'rules'    => [],
		'messages' => [],
	],

	'agency' => [
		'rules'    => [
			'id_number'          => 'required',
			'tax_number'         => 'required',
			'responsible_person' => 'required',
			'contact_person'     => 'required',
			'address'            => 'required',
			'phone'              => 'required',
			'area_id'            => 'required',
			'city_id'            => 'required',
		],
		'messages' => [],
	],

	'broker' => [
		'rules'    => [

		],
		'messages' => [

		],
	],
];