<?php


return [
	'application_name'  =>  'It\'s Something',
    'author_name'       =>  'Borche Bojcheski',
    'author_email'      =>  'borche@bojcheski.com',
    'html'  =>  [
    	'title' =>  'It\'s Something',
        'meta'  =>  [
        	'description'   =>  'It\'s Something',
            'author'        =>  'Someone',
        ]
    ],
];