<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\User\Entities\User;

class UserRegistered extends Mailable {

	use Queueable, SerializesModels;

	public $user;

	/**
	 * Create a new message instance.
	 *
	 * @param User $user
	 */
	public function __construct (User $user) {
		$this->user = $user;
		//
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build () {
		return $this->view('Helpers::mail.user.registered');
	}
}
