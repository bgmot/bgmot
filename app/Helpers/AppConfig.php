<?php

namespace Helpers;

use Auth;
use Config;

class AppConfig {

	private $request = null;

	private $config = [
		'log',
		'mimes',
		'options',
		'sending',
		'settings',
		'template',
	];

	private $configs = [];

	private $user = null;

	/**
	 * AppConfig constructor.
	 */
	public function __construct ($request = null) {
		foreach ($this->config as $config) {
			try {
				$this->configs[$config] = Config::get('platform.' . $config);
			} catch (\Exception $e) {

			}
		}

		if (!is_null($request)) {
			$this->setRequest($request);
		}

		$this->user = Auth::user();
	}

	public function get ($config = 'settings', $key = null) {

		if (is_null($key)) {
			return '';
		}

		if (!array_key_exists($config, $this->configs)) {
			return '';
		}

		return isset($this->configs[$config][$key]) ? $this->configs[$config][$key] : '';
	}

	public function setUser ($user) {
		$this->user = $user;

		return $this;
	}

	public function getUser () {
		return $this->user;
	}

	public function getRequest () {
		return $this->request;
	}

	public function setRequest ($request) {
		$this->request = $request;

		return $this;
	}

	public function hasAccessToModule ($module = null) {
		$this->setRequest(request());

		if (is_null($module)) {
			return false;
		}

		if (!is_null($this->getUser()) && $this->getUser()->is_main == 1) {
			return true;
		}

		if (!is_null($this->getRequest())) {
			if (isset($this->getRequest()->_control) && array_key_exists('modules', $this->getRequest()->_control)) {
				foreach ($this->getRequest()->_control['modules'] as $accessModule) {
					if ($accessModule['name'] == $module) {
						return true;
					}
				}
			}
		}

		return false;
	}

}