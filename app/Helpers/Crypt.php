<?php

namespace Helpers;

use Config;

class Crypt {

	// default data.
	// keep separate into platform/options.php config file for easier maintenance

	private static $cryptMethod = 'AES-256-CBC';
	private static $cryptKey = 's[xNKgx*}1#08iS^m]!GzhT1s9L{p%Tb';
	private static $cryptHashAlgo = 'sha512';
	private static $cryptIvSeparator = '::';

	public static function encrypt ($data = null) {

		$crypt = false;
		$isObject = false;

		if (is_null($data)) {
			return $crypt;
		}

		if (is_object($data) || is_array($data)) {
			// we will forecefully json_encode BEFORE and attach ::1 or ::2 to the base64 encoded string so when
			// we decode we will recreate the object (by json_encode tho)

			$data = json_encode($data);
			if (is_object($data)) {
				$isObject = 1;
			} else if (is_array($data)) {
				$isObject = 2;
			}
		}

		$config = Config::get('platform.options');

		// these are default ones
		$cryptKey = self::$cryptKey;
		$cryptMethod = self::$cryptMethod;
		$cryptHashAlgo = self::$cryptHashAlgo;
		$cryptIvSeparator = self::$cryptIvSeparator;

		if (isset($config['crypt']['key']) && trim($config['crypt']['key'])) {
			$cryptKey = $config['crypt']['key'];
		}

		if (isset($config['crypt']['method']) && trim($config['crypt']['method'])) {
			$cryptMethod = $config['crypt']['method'];
		}

		if (isset($config['crypt']['hashAlgo']) && trim($config['crypt']['hashAlgo'])) {
			$cryptHashAlgo = $config['crypt']['hashAlgo'];
		}

		if (isset($config['crypt']['ivSeparator']) && trim($config['crypt']['ivSeparator'])) {
			$cryptIvSeparator = $config['crypt']['ivSeparator'];
		}

		try {
			if (function_exists('openssl_encrypt')) {
				// makes shorter encrypted strings :)
				$key = hash($cryptHashAlgo, $cryptKey);

				// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
				$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cryptMethod));

				$crypt = trim(base64_encode(openssl_encrypt($data, $cryptMethod, $key, 0, $iv)));
				if ($isObject) {
					$crypt = base64_encode($crypt . $cryptIvSeparator . $iv . $cryptIvSeparator . $isObject);
				} else {
					$crypt = base64_encode($crypt . $cryptIvSeparator . $iv);
				}
			} else {
				$crypt = \Crypt::encrypt($data);
			}
		} catch (\Exception $e) {
			// Log the error here
			d($e->getMessage());
		}

		return base64_encode($crypt);
	}

	public static function decrypt ($data = null) {
		$decrypt = false;

		if (is_null($data)) {
			return $decrypt;
		}

		$config = Config::get('platform.options');

		// these are default ones
		$cryptKey = self::$cryptKey;
		$cryptMethod = self::$cryptMethod;
		$cryptHashAlgo = self::$cryptHashAlgo;
		$cryptIvSeparator = self::$cryptIvSeparator;

		if (isset($config['crypt']['key']) && trim($config['crypt']['key'])) {
			$cryptKey = $config['crypt']['key'];
		}

		if (isset($config['crypt']['method']) && trim($config['crypt']['method'])) {
			$cryptMethod = $config['crypt']['method'];
		}

		if (isset($config['crypt']['hashAlgo']) && trim($config['crypt']['hashAlgo'])) {
			$cryptHashAlgo = $config['crypt']['hashAlgo'];
		}

		if (isset($config['crypt']['ivSeparator']) && trim($config['crypt']['ivSeparator'])) {
			$cryptIvSeparator = $config['crypt']['ivSeparator'];
		}

		try {
			if (function_exists('openssl_decrypt')) {
				$key = hash($cryptHashAlgo, $cryptKey);

				// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning

				$data = base64_decode(base64_decode($data));
				$parts = explode($cryptIvSeparator, $data);

				$isObject = false;

				if (count($parts) == 3) {
					$isObject = $parts[2];
				}

				$data = $parts[0];
				$iv = $parts[1];

				$decrypt = trim(openssl_decrypt(base64_decode($data), $cryptMethod, $key, 0, $iv));

				if ($isObject) {
					if ($isObject == 1) {
						// this is object
						$decrypt = json_decode($decrypt);
					} else {
						// this is array
						$decrypt = json_decode($decrypt, true);
					}
				}
			} else {
				$decrypt = \Crypt::decrypt($data);
			}
		} catch (\Exception $e) {
			// Log the error here
			d($e->getMessage());
		}

		return $decrypt;
	}
}