<?php

namespace Helpers;

use Carbon\Carbon;

class Validate {

	private static function ip ($ip, $type = 4) {
		return filter_var($ip, FILTER_VALIDATE_IP) && filter_var($ip, FILTER_VALIDATE_IP, (($type == 4) ? FILTER_FLAG_IPV4 : FILTER_FLAG_IPV6));
	}

	public static function ip4($ip) {
		return self::ip($ip, 4);
	}

	public static function ip6($ip) {
		return self::ip($ip, 6);
	}

	public function is_ip($ip) {
		return filter_var($ip, FILTER_VALIDATE_IP);
	}

	public static function is_between (Carbon $date, Carbon $from, Carbon $to) {
		return ($date->between($from, $to));
	}

	public static function is_collection ($var = null) {
		return (bool) (!is_null($var) && gettype($var) == 'object' && is_object($var) && (get_class($var) == 'Illuminate\Database\Eloquent\Collection' || get_class($var) == 'Illuminate\Support\Collection'));
	}

	public static function is_uuid($var = null) {
		return (bool) (UUID::import($var)->version != 0);
	}

	public static function is_json ($string) {
		return (is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE)) ? true : false;
	}

	public static function is_timestamp ($timestamp) {
		$check = (is_int($timestamp) OR is_float($timestamp))
			? $timestamp
			: (string) (int) $timestamp;

		return ($check === $timestamp)
		AND ((int) $timestamp <= PHP_INT_MAX)
		AND ((int) $timestamp >= ~PHP_INT_MAX);
	}

}