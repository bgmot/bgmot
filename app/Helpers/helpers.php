<?php

use Helpers\Helper;
use Helpers\Log;
use Helpers\UUID;
use Illuminate\Support\Debug\Dumper;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Formatter\LineFormatter;
use Monolog\Processor\WebProcessor;

//use Config;

if (!function_exists('d')) {
	function d () {
		array_map(function ($x) {
			(new Dumper)->dump($x);
		}, func_get_args());
	}
}

if (!function_exists('logs_path')) {
	function logs_path ($path = '') {
		return app('path.storage') . '/logs' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
	}
}

if (!function_exists('load_assets')) {
	function load_assets ($request = null, $type = 'css', $location = 'header') {

		if (is_null($request)) {
			return '';
		}

		if (!isset($request->_routeSettings) || is_null($request->_routeSettings)) {
			return '';
		}

		$currentModule = $request->_routeSettings;

		if (Cache::store('redis-local')
		         ->has('module_assets_' . $currentModule->module_uuid)
		) {
			$assets = json_decode(Cache::store('redis-local')
			                           ->get('module_assets_' . $currentModule->module_uuid));
		} else {
			$module = Module::find($currentModule->module_id);
			$assets = $module->asset->toJson();

			Cache::store('redis-local')
			     ->add('module_assets_' . $module->uuid, $assets, Config::get('platform.options.cache.routes'));
		}

		if (is_array($assets)) {
			$assets = collect($assets);
		} else {
			$assets = collect();
		}

		$assetsToLoad = $assets->where('asset_location', $location)
		                       ->where('asset_type', $type)
		                       ->where('is_enabled', 1);

		$html = '';
		foreach ($assetsToLoad as $asset) {
			if ($asset->asset_type == 'css') {
				$html .= sprintf(Helper::CSS, $asset->path);
			} else {
				$html .= sprintf(Helper::JS, $asset->path);
			}
		}

		return $html;
	}
}

if (!function_exists('get_controllers')) {
	function get_controllers ($module = null) {
		$methods = [];
		$prohibitedMethods = [
			'__construct',
		];

		if ($module == null) {
			foreach (glob(base_path() . '/modules/*') as $_module) {
				foreach (glob($_module . '/Http/Controllers/*Controller.php') as $controller) {
					$class = str_replace(base_path() . '/', '', $controller);
					$class = str_replace('/', '\\', $class);
					$class = str_replace('modules', 'Modules', $class);
					$x = new \ReflectionClass(str_replace('.php', '', $class));
					foreach ($x->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
						if (!in_array($method->name, $prohibitedMethods)) {
							if ($method->class == str_replace('.php', '', $class)) {
								$methods[str_replace(base_path() . '/modules/', '', $_module)][$x->getShortName()][] = $method->name;
							}
						}
					}
				}
			}
		} else {
			foreach (glob(base_path() . '/modules/' . $module . '/Http/Controllers/*Controller.php') as $controller) {
				$class = str_replace(base_path() . '/', '', $controller);
				$class = str_replace('/', '\\', $class);
				$class = str_replace('modules', 'Modules', $class);
				$x = new \ReflectionClass(str_replace('.php', '', $class));
				foreach ($x->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
					if (!in_array($method->name, $prohibitedMethods)) {
						if ($method->class == str_replace('.php', '', $class)) {
							$methods[$module][$x->getShortName()][] = $method->name;
						}
					}
				}
			}
		}

		return $methods;
	}
}

if (!function_exists('get_entities')) {
	function get_entities ($module = null) {
		$entities = [];

		if ($module == null) {
			foreach (glob(base_path() . '/modules/*') as $_module) {
				foreach (glob($_module . '/Entities/*.php') as $controller) {
					$class = str_replace(base_path() . '/', '', $controller);
					$class = str_replace('/', '\\', $class);
					$class = str_replace('modules', 'Modules', $class);

					if (class_exists(str_replace('.php', '', $class))) {
						$x = new \ReflectionClass(str_replace('.php', '', $class));
						$entities[$_module][$x->getShortName()] = $x->getNamespaceName();
					}
				}
			}
		} else {
			foreach (glob(base_path() . '/modules/' . $module . '/Entities/*.php') as $controller) {
				$class = str_replace(base_path() . '/', '', $controller);
				$class = str_replace('/', '\\', $class);
				$class = str_replace('modules', 'Modules', $class);
				if (class_exists(str_replace('.php', '', $class))) {
					$x = new \ReflectionClass(str_replace('.php', '', $class));
					$entities[$module][$x->getShortName()] = $x->getNamespaceName();
				}
			}
		}

		return $entities;
	}
}

if (!function_exists('getRouteByParams')) {
	function getRouteByParams ($module, $controller, $action, $parameters = [], $absolute = true) {
		return Helper::getRoute(strtolower($module . '.' . $controller . '.' . $action), $parameters, $absolute);
	}
}

if (!function_exists('getRoute')) {
	function getRoute ($name, $parameters = [], $absolute = true) {
		return Helper::getRoute($name, $parameters, $absolute);
	}
}

if (!function_exists('getRandomUUID')) {
	function getRandomUUID () {
		return sha1(md5(sha1(UUID::generate(4))));
	}
}

if (!function_exists('getUUID')) {
	function getUUID ($version = 1) {
		return (string) UUID::generate($version);
	}
}