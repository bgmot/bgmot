<?php

namespace Helpers;

use Faker\Factory;

class Random {

	public static function name ($amount = 1) {
		$faker = Factory::create();
		$names = [];
		$genders = [
			'male',
			'female',
		];
		for ($i = 0; $i < $amount; $i++) {
			$hasApostrophe = true;
			while ($hasApostrophe) {
				$name = $faker->firstName($genders[array_rand($genders)]) . ' ' . $faker->lastName;
				$hasApostrophe = (stristr($name, "'") == true) ? true : false;
			}

			//$name = $faker->firstName($genders[array_rand($genders)]) . ' ' . $faker->lastName;

			$names[] = [
				'name'  => $name,
				'email' => strtolower(str_replace(' ', '.', $name)),
			];
		}

		return ($amount == 1 && count($names) == 1) ? $names[0] : $names;
	}

	public static function firstAndLast ($amount = 1) {
		$faker = Factory::create();
		$names = [];
		$genders = [
			'male',
			'female',
		];
		for ($i = 0; $i < $amount; $i++) {
			$hasApostrophe = true;
			while ($hasApostrophe) {
				$name = $faker->firstName($genders[array_rand($genders)]) . ' ' . $faker->lastName;
				$hasApostrophe = (stristr($name, "'") == true) ? true : false;
			}

			list($firstName, $lastName) = explode(' ', $name);
			$names[] = [
				'firstName' => ucwords(strtolower($firstName)),
				'lastName'  => ucwords(strtolower($lastName)),
			];
		}

		return ($amount == 1 && count($names) == 1) ? $names[0] : $names;
	}

	public static function splitAddress ($amount = 1) {
		$faker = Factory::create();
		$addresses = [];

		for ($i = 0; $i < $amount; $i++) {
			$fullAddress = $faker->address;
			$add = explode("\n", $fullAddress);

			$address = $add[0];
			$cc = explode(',', $add[1]);

			$city = $cc[0];
			$bb = explode(' ', trim($cc[1]));

			$country = 'USA';
			$state = trim($bb[0]);
			$zip = trim($bb[1]);

			if (stristr($zip, '-')) {
				$_zz = explode('-', $zip);
				$zip = $_zz[0];
			}

			$addresses[] = [
				'full_address' => $fullAddress,
				'address'      => $address,
				'city'         => $city,
				'country'      => $country,
				'state'        => $state,
				'zip'          => $zip,
			];
		}

		return ($amount == 1 && count($addresses) == 1) ? $addresses[0] : $addresses;
	}

	public static function address ($amount = 1) {
		$faker = Factory::create();
		$addresses = [];

		for ($i = 0; $i < $amount; $i++) {
			$addresses[] = $faker->address;
		}

		return ($amount == 1 && count($addresses) == 1) ? $addresses[0] : $addresses;
	}

	public static function string ($limit = 32) {
		return sha1((string) UUID::generate(1) . '_' . uniqid('', true) . '_' . str_random($limit));
	}
}