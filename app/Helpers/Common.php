<?php

namespace Helpers;

use Config;

class Common {

	/**
	 * Common::getQueriesFromSqlFile()
	 *
	 * @param string $sqlFile
	 * @param string $dbPrefix
	 * @return array
	 */
	public static function getQueriesFromSqlFile ($sqlFile, $dbPrefix = null) {
		if (!is_file($sqlFile) || !is_readable($sqlFile)) {
			return [];
		}

		if (!empty($dbPrefix)) {
			$searchReplace = [
				'CREATE TABLE IF NOT EXISTS `' => 'CREATE TABLE IF NOT EXISTS `' . $dbPrefix,
				'DROP TABLE IF EXISTS `'       => 'DROP TABLE IF EXISTS `' . $dbPrefix,
				'INSERT INTO `'                => 'INSERT INTO `' . $dbPrefix,
				'ALTER TABLE `'                => 'ALTER TABLE `' . $dbPrefix,
				'REFERENCES `'                 => 'REFERENCES `' . $dbPrefix,
				'UPDATE `'                     => 'UPDATE `' . $dbPrefix,
				'DELETE FROM `'                => 'DELETE FROM `' . $dbPrefix,
			];
			$search = array_keys($searchReplace);
			$replace = array_values($searchReplace);
		}

		$queries = [];
		$query = '';
		$lines = file($sqlFile);

		foreach ($lines as $line) {

			if (empty($line) || strpos($line, '--') === 0 || strpos($line, '#') === 0 || strpos($line, '/*!') === 0) {
				continue;
			}

			$query .= $line;

			if (!preg_match('/;\s*$/', $line)) {
				continue;
			}

			if (!empty($dbPrefix)) {
				$query = str_replace($search, $replace, $query);
			}

			if (!empty($query)) {
				$queries[] = $query;
			}

			$query = '';
		}

		return $queries;
	}

	/**
	 * Common::functionExists()
	 *
	 * @param string $name
	 * @return bool
	 */
	public static function functionExists ($name) {
		static $_exists = [];
		static $_disabled = null;
		static $_shDisabled = null;

		if (isset($_exists[$name]) || array_key_exists($name, $_exists)) {
			return $_exists[$name];
		}

		if (!function_exists($name)) {
			return $_exists[$name] = false;
		}

		if ($_disabled === null) {
			$_disabled = ini_get('disable_functions');
			$_disabled = explode(',', $_disabled);
			$_disabled = array_map('trim', $_disabled);
		}

		if (is_array($_disabled) && in_array($name, $_disabled)) {
			return $_exists[$name] = false;
		}

		if ($_shDisabled === null) {
			$_shDisabled = ini_get('suhosin.executor.func.blacklist');
			$_shDisabled = explode(',', $_shDisabled);
			$_shDisabled = array_map('trim', $_shDisabled);
		}

		if (is_array($_shDisabled) && in_array($name, $_shDisabled)) {
			return $_exists[$name] = false;
		}

		return $_exists[$name] = true;
	}

	/**
	 * Common::findPhpCliPath()
	 *
	 * @return string
	 */
	public static function findPhpCliPath () {
		static $cliPath;

		if ($cliPath !== null) {
			return $cliPath;
		}

		$cliPath = '/usr/bin/php';

		if (!self::functionExists('exec')) {
			return $cliPath;
		}

		$variants = ['php-cli', 'php5-cli', 'php5', 'php', 'php7', 'php7.0', 'php7-cli', 'php7.0-cli'];
		foreach ($variants as $variant) {
			$out = @exec(sprintf('command -v %s 2>&1', $variant), $lines, $status);
			if ($status != 0 || empty($out)) {
				continue;
			}
			$cliPath = $out;
			break;
		}

		return $cliPath;
	}

	public static function mimes ($extension = null) {
		$mimes = Config::get('platform.mimes');
		if (!is_null($extension)) {
			return isset($mimes[$extension]) ? [$extension => $mimes[$extension]] : [];
		}

		return $mimes;
	}

	public static function extensions ($mime = null) {
		$mimes = Config::get('platform.mimes');
		if (!is_null($mime)) {
			$exts = [];
			foreach ($mimes as $ext => $m) {
				if (in_array(strtolower($mime), $m)) {
					$exts[] = $ext;
				}
			}

			return $exts;
		}

		return array_keys($mimes);
	}

	public static function isMimeAllowed ($mime = null) {
		if (is_null($mime)) {
			return false;
		}

		$mimes = Config::get('platform.mimes');

		foreach ($mimes as $ext => $m) {
			if (in_array(strtolower($mime), $m)) {
				return true;
			}
		}

		return false;
	}

	public static function isExtensionAllowed ($extension = null) {
		if (is_null($extension)) {
			return false;
		}

		$mimes = Config::get('platform.mimes');

		return array_key_exists(strtolower($extension), $mimes);
	}

	public static function camelize ($input) {
		$words = explode('_', strtolower($input));

		$return = '';
		foreach ($words as $word) {
			$return .= ucfirst(trim($word));
		}

		return $return;
	}

	public static function decamelize ($input) {
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
		$ret = $matches[0];
		foreach ($ret as &$match) {
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}

		return implode('_', $ret);
	}

	public static function getEmptyAjaxResponse() {
		return [
			'status'    =>  true,
		    'error'     =>  false,
		    'message'   =>  '',
		    'response'  =>  [],
		];
	}

	public static function convertStringTo($type = 'boolean', $value) {
		if ($type == 'phone') {
			return preg_replace('/[^[:alnum:]]/', '', $value);
		}

		if ($type == 'date') {
			return date('Y-m-d H:i:s', strtotime($value));
		}

		if ($type == 'boolean') {
			return ($value == 'true') ? true : false;
		}

		if ($type == 'empty') {
			return ($value == '') ? '' : $value;
		}

		if ($type == 'decimal') {
			return floatval($value);
		}

		if ($type == 'integer') {
			return (is_null($value) || $value == '') ? 0 : intval($value);
		}

		if (is_bool($value)) {
			return ($value == true) ? 'true' : 'false';
		}

		if (is_null($value) || trim($value) == '') {
			return 'null';
		}
	}
}