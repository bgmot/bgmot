<?php

namespace Helpers;

use Config;

class Helper {

	const CSS = "\t<link href=\"%s\" rel=\"stylesheet\" type=\"text/css\" />\n";
	const JS = "\t<script src=\"%s\"></script>\n";

	public static function getDataURI ($image, $mime = '') {
		return 'data:' . $mime . ';base64,' . base64_encode(file_get_contents($image));
	}

	public static function str_random_own ($length = 32, $use_numbers = true, $use_special = false) {
		$letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$numbers = '1234567890';
		$special = "!@#$%^&*()_+=-][}{';\":/.,<>?";
		$characters = $letters;

		if ($use_numbers) {
			$characters .= $numbers;
		}

		if ($use_special) {
			$characters .= $special;
		}

		return substr(str_shuffle($characters), 0, 1) . substr(str_shuffle($characters), 0, intval($length - 1));
	}

	public static function str_cut ($str, $char, $pos) {
		$i = 0;
		$a = explode($char, $str);
		$r = [];
		foreach ($a as $b) {
			if ($pos > $i) {
				$r[] = $b;
			}
			$i++;
		}

		return implode($char, $r);
	}

	public static function getHost ($withHttp = false) {
		$parts = parse_url(Config::get('app.domain'));

		return ((!$withHttp) ? '' : $parts['scheme'] . '//') . $parts['host'];
	}

	public static function getRoute ($name, $parameters = [], $absolute = true) {
		//, ['domain' => $request->engine]

		if (!array_key_exists('domain', $parameters)) {
			$request = request();

		}

		//dd($name, $parameters);

		return route($name, $parameters, $absolute);
	}

	public static function routeExists ($route) {

	}
}