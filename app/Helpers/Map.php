<?php

namespace Helpers;

// Class that will generate the map for displaying
use Modules\Backend\Entities\Area;

class Map {

	public static function generate () {
		$buttons = '<div class="buttons-row" id="dealType">
            <a href="' . route('search.search', ['offer' => 'sale']) . '" class="btn"><span>' . __('translate.property.sale') . '</span></a>
            <a href="' . route('search.search', ['offer' => 'rent']) . '" class="btn"><span>' . __('translate.property.rent') . '</span></a>
            <a href="' . route('search.search', ['offer' => 'trade']) . '" class="btn"><span>' . __('translate.property.trade') . '</span></a>
            <a href="' . route('search.search', ['offer' => 'roommate']) . '" class="btn"><span>' . __('translate.property.roommate') . '</span></a>
            <a href="' . route('search.search', ['offer' => 'construction']) . '" class="btn"><span>' . __('translate.property.construction') . '</span></a>
        </div>';

		$cities = $areas = '';

		foreach (Area::where('country_id', 1)
		             ->get() as $area) {
			$areas .= self::generateArea($area);
			foreach ($area->city as $city) {
				$cities .= self::generateCity($city);
			}
		}

		$mapSvg = '<?xml version="1.0" encoding="utf-8"?>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_2" x="0px" y="0px" viewBox="0 0 731.5 493.9" style="enable-background:new 0 0 731.5 493.9;" xml:space="preserve">
                        
                    <path class="fon" d="M710.2,128.3l0.9-23.8l-40.8-3.4l-22-10.8l-3.4-17.8l-9.4-8.9l-18.6,6.7l-2.5-5.2l0,0c-1.6-3.3-1.8-3.6-2.1-4  l-0.3-0.3l-0.3-0.3c-1.7-1.7-4.4-2.6-7.9-2.6h-3.6l-2.5,4.7l-14.5,0.1l-2.3-4.9l-7.3-2.9l-4.7-7.3l-24.1-5l-26.3,6.7L495,57.1  l-23.3,2.3l-4.5,0.5l-24.5,8.5l-18.9,17.1l-23,25.1l-4.2,0.6l0.4-0.6L365.6,121l-21.5-11.6l-2.2,0.5c-2.4,0.5-5.1,1-6.5,1.2  c-2-0.9-5.5-2.2-12.2-4.3l-1.4-0.5l-26.3,4.8l-10.2-6.6l-17.2-4.6l-15.1,10.4l-6.8,0.5l0,0.2l-10,0.8l-14.3-6.4l-33.2-11.5  l-13.3,1.5l-11.7-6l-27-2.1l-14.4,6.8l-10-2.2l0,0.1l-8.6-1.8h-4.1l-2.1-3l2.9-9.4l3-3.2h7.3l10.8-14.9l-7.5-9.3l-13.1-1.6  L86.6,35.3l-18.1-2.8l-7.3,11.2l0.2,13.7l-6.2,5l-14,2.8l-6.9,32.9l5.7,21.8l8.5,4.4l0.2,6.1l4.7,20.5l14.2,11.4l11.8,4.8l11.9,19.7  l11.4,5.6l-0.6,6.1l-20.7,19l-3.9,10.7l-21,1.6l-14.3,14.8l7.5,14l-6.1,25.2L55,294l-1.5,5.4l-6.2,10.4l-13.1,6.3l18.3,29.4  L73.1,359l16.8,7l10.2,34.7l6.4,1.2l0.3,1.7l-8.6,9.7l1.5,52.2l33.9,0.3l6.8-9.3l5.9,1.7l8.8-3.1l23.5,2.7l18.3-8l17.6-0.8l13.7-4.4  l0.7-7.8l10.5,4.3l5.2-6.7l3.4,4.4l18.7-2.5l3.8-1.6l5.7,16.1l12.7,2.3l14.6,12.7l4.9-9.5l5.8-0.3l19.6,8.8l10.9,9.4l12,3.7l26.1-10  h13.8l12.7-4l8.9,4.1l3.3-0.7l0,0.1l24.3-5.6l9.4-17.2l-4.4-24.4l-7.4-10.3l2.9-2.2l20.9,4.1l5.2-15.6l18-1.6l7.6-15l-1-3.8l2-2.8  l6.2,2.4l32.7-8.2l6.8-9.2l10.8,2.5l10.1-5.3l6.1,4.1l13.2,15.4l11.3,8l16.6-8.4l15.2,1.7l3.4-5.6l5.4,2.2l20.5-9.4l-33.4-38.3  l2.3-9.4L608,292.7l-14-1.4l1.2-2.1l14.1-2.7l1.2-10l8.2-6l1.8-4.5h19.2l-0.8-20.6l-1.3-32.9l6.3-21.2l5.2-0.7l0.8-1.6l0,0  l11.3-22.2l5.8,0.1l11.3-4.1l17.8,8.2l18.7-30.4L710.2,128.3z M338.5,113.3L338.5,113.3C338.4,113.3,338.4,113.3,338.5,113.3  C338.4,113.3,338.4,113.3,338.5,113.3z"/>
                    <g id="areas">
					' . $areas . '                    
                    </g>
                    </g>
                    <g id="cities">
                    ' . $cities . '
                    </g>
                </svg>';

		return $mapSvg . $buttons;
	}

	private static function generateArea ($element) {
		$svg = '';

		$data = json_decode($element->country_map, true);

		foreach ($data as $tag => $tags) {
			foreach ($tags as $t) {
				$_tempSvg = '<' . $tag . ' class="tooltiped area-selector st0" data-id="' . $element->id . '" data-title="' . $element->name . '"';

				foreach ($t as $attributeName => $attributeValue) {
					$_tempSvg .= ' ' . $attributeName . '="' . $attributeValue . '"';
				}

				$_tempSvg .= '>';

				$_tempSvg .= '</' . $tag . '>';
			}

			$svg .= $_tempSvg;
		}

		return $svg;
	}

	private static function generateCity ($element) {
		$svg = '<g class="tooltiped city-selector" data-title="' . $element->name . '" data-id="' . $element->id . '" >';

		$data = json_decode($element->country_map, true);

		foreach ($data as $tag => $tags) {
			foreach ($tags as $t) {
				$_tempSvg = '<' . $tag;

				foreach ($t as $attributeName => $attributeValue) {
					$_tempSvg .= ' ' . $attributeName . '="' . $attributeValue . '"';
				}

				$_tempSvg .= '>';

				$_tempSvg .= '</' . $tag . '>';
			}

			$svg .= $_tempSvg;
		}

		return 'data-title>="' . $element->name . '" ' . $svg . '</g>';
	}
}