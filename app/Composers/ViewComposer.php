<?php namespace App\Composers;

use File;
use Helpers\AppConfig;
use Illuminate\Contracts\View\View;
use Config;
use Modules\Campaign\Model\Campaign;
use Modules\Trigger\Model\Trigger;
use Route;
use Symfony\Component\HttpFoundation\Request;
use Session;

class ViewComposer {

	/**
	 * Create a new profile composer.
	 *
	 * @return void
	 */
	public function __construct () {
	}

	/**
	 * Bind data to the view.
	 *
	 * @param  View $view
	 * @return void
	 */
	public function compose (View $view) {

		$request = Route::getCurrentRequest();
		$view->with('request', $request);

		$appConfig = new AppConfig();
		$view->with('appConfig', $appConfig);

	}

}