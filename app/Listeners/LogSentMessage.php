<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Queue\InteractsWithQueue;
use Log;

class LogSentMessage implements ShouldQueue {

	use InteractsWithQueue;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct () {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param MessageSent $event
	 * @return void
	 */
	public function handle (MessageSent $event) {
		Log::info('[' . now() . '] SentMessage : ' . json_encode($event));
	}
}
