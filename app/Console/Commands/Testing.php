<?php

namespace App\Console\Commands;

use DB;
use Helpers\Map;
use Illuminate\Console\Command;
use Modules\Backend\Entities\Area;
use Modules\Backend\Entities\City;

class Testing extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'testing';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct () {
		parent::__construct();

		set_time_limit(0);

		ini_set('memory_limit', '3096M');
	}

	public function handle () {

		return $this->processSvg();
		return $this->generateMap();

	}

	private function generateMap() {
		$xx = Map::generate();
	}

	private function processSvg () {
		$svg = '
                        <g id="София_1_" class="tooltiped city-selector" onclick="changecity(this, 697,992)">
                            <path class="st0"
                                  d="M146.7,248c-5.1,0-9.1,4.2-9.1,9.1c0,5.1,4.2,9.1,9.1,9.1c5,0,9.1-4.2,9.1-9.1C156,252.2,151.8,248,146.7,248    z"/>
                            <rect id="XMLID_80_" x="142.2" y="252.6" class="st1" width="9.1" height="9.1"/>
                            <path id="XMLID_79_" class="st1"
                                  d="M146.7,271c-7.5,0-13.7-6.2-13.7-13.7s6.2-13.7,13.7-13.7s13.7,6.2,13.7,13.7    C160.4,264.8,154.4,271,146.7,271z M146.7,248c-5.1,0-9.1,4.2-9.1,9.1c0,5.1,4.2,9.1,9.1,9.1c5,0,9.1-4.2,9.1-9.1    C156,252.2,151.8,248,146.7,248z"/>
                            <title>Град София</title>
                        </g>
                        <g id="Пазарджик_1_" class="tooltiped city-selector" onclick="changecity(this, 2587,2588)">
                            <circle class="st0" cx="253.3" cy="335.3" r="8.5"/>
                            
                                <rect id="XMLID_78_" x="250.4" y="332.4" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_77_" class="st1"
                                      d="M253.3,343.8c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5c4.7,0,8.5,3.8,8.5,8.5     S258,343.8,253.3,343.8z M253.3,328.8c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5c3.6,0,6.5-2.9,6.5-6.5S256.9,328.8,253.3,328.8z"/>
                            
                            <title>Град Пазарджик</title></g>
                        <g id="Пловдив_1_" class="tooltiped city-selector" onclick="changecity(this, 3003,3004)">
                            <circle class="st0" cx="297.3" cy="341.8" r="8.5"/>
                            
                                <rect id="XMLID_75_" x="294.4" y="339" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_74_" class="st1"
                                      d="M297.3,350.4c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S302,350.4,297.3,350.4     z M297.3,335.3c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5C303.8,338.3,300.9,335.3,297.3,335.3z"/>
                            
                            <title>Град Пловдив</title></g>
                        <g id="Стара_Загора_1_" class="tooltiped city-selector" onclick="changecity(this, 4030,4031)">
                            <circle class="st0" cx="392" cy="301.7" r="8.6"/>
                            <rect id="XMLID_72_" x="389.1" y="298.8" class="st1" width="5.8" height="5.8"/>
                            <path id="XMLID_71_" class="st1"
                                  d="M392,310.2c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S396.7,310.2,392,310.2z      M392,295.2c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S395.6,295.2,392,295.2z"/>
                            <title>Град Стара Загора</title>
                        </g>
                        <g id="Сливен_1_" class="tooltiped city-selector" onclick="changecity(this, 3674,3675)">
                            <circle class="st0" cx="469.3" cy="263" r="8.5"/>
                           
                                <rect id="XMLID_69_" x="466.4" y="260.1" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_68_" class="st1"
                                      d="M469.3,271.5c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     C477.7,267.7,474,271.5,469.3,271.5z M469.3,256.5c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5     S472.9,256.5,469.3,256.5z"/>
                           
                            <title>Град Сливен</title></g>
                        <g id="Ямбол_1_" class="tooltiped city-selector" onclick="changecity(this, 5204,5205)">
                            <circle class="st0" cx="490.2" cy="293.9" r="8.5"/>
                           
                                <rect id="XMLID_66_" x="487.3" y="291.1" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_65_" class="st1"
                                      d="M490.2,302.4c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S494.9,302.4,490.2,302.4z M490.2,287.4c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5     C496.7,290.4,493.8,287.4,490.2,287.4z"/>
                           
                            <title>Град Ямбол</title></g>
                        <g id="Бургас_1_" class="tooltiped city-selector" onclick="changecity(this, 735,991)">
                            <circle class="st0" cx="579.4" cy="285.5" r="8.5"/>
                           
                                <rect id="XMLID_63_" x="576.5" y="282.6" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_62_" class="st1"
                                      d="M579.4,294c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S584.1,294,579.4,294z      M579.4,279c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5C586,281.9,583.1,279,579.4,279z"/>
                           
                            <title>Град Бургас</title></g>
                        <g id="Варна_1_" class="tooltiped city-selector" onclick="changecity(this, 4434,4435)">
                            <circle class="st0" cx="634.1" cy="177.3" r="8.5"/>
                           
                                <rect id="XMLID_60_" x="631.2" y="174.5" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_59_" class="st1"
                                      d="M634.1,185.8c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S638.7,185.8,634.1,185.8z M634.1,170.8c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S637.7,170.8,634.1,170.8z"/>
                           
                            <title>Град Варна</title></g>
                        <g id="Шумен_1_" class="tooltiped city-selector" onclick="changecity(this, 3405,3406)">
                            <circle class="st0" cx="534.5" cy="176.4" r="8.5"/>
                           
                                <rect id="XMLID_57_" x="531.6" y="173.6" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_56_" class="st1"
                                      d="M534.5,184.9c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S539.2,184.9,534.5,184.9z M534.5,169.9c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5     C541,172.9,538.1,169.9,534.5,169.9z"/>
                           
                            <title>Град Шумен</title></g>
                        <g id="Търговище_1_" class="tooltiped city-selector" onclick="changecity(this, 4236,4237)">
                            <circle class="st0" cx="496.3" cy="181" r="8.6"/>
                           
                                <rect id="XMLID_54_" x="493.4" y="178.1" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_53_" class="st1"
                                      d="M496.3,189.5c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S501,189.5,496.3,189.5     z M496.3,174.5c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5C502.9,177.4,500,174.5,496.3,174.5z"/>
                           
                            <title>Град Търговище</title></g>
                        <g id="Велико_Търново_1_" class="tooltiped city-selector" onclick="changecity(this, 4603,4604)">
                            <circle class="st0" cx="395" cy="204.1" r="8.5"/>
                           
                                <rect id="XMLID_51_" x="392" y="201.2" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_50_" class="st1"
                                      d="M395,212.6c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     C403.4,208.8,399.6,212.6,395,212.6z M395,197.6c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S398.6,197.6,395,197.6z     "/>
                           
                            <title>Град Велико Търново</title></g>
                        <g id="Габрово_1_" class="tooltiped city-selector" onclick="changecity(this, 1212,1213)">
                            <circle class="st0" cx="360.6" cy="238.1" r="8.4"/>
                           
                                <rect id="XMLID_48_" x="357.7" y="235.2" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_47_" class="st1"
                                      d="M360.6,246.6c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S365.3,246.6,360.6,246.6z M360.6,231.6c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S364.2,231.6,360.6,231.6z"/>
                           
                            <title>Град Габрово</title></g>
                        <g id="Ловеч_1_" class="tooltiped city-selector" onclick="changecity(this, 2474,2475)">
                            <circle class="st0" cx="294.4" cy="198.5" r="8.5"/>
                           
                                <rect id="XMLID_45_" x="291.5" y="195.7" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_44_" class="st1"
                                      d="M294.4,207c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S299,207,294.4,207z      M294.4,192c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S298,192,294.4,192z"/>
                           
                            <title>Град Ловеч</title></g>
                        <g id="Плевен_1_" class="tooltiped city-selector" onclick="changecity(this, 2879,2880)">
                            <circle class="st0" cx="285.9" cy="157" r="8.5"/>
                           
                                <rect id="XMLID_42_" x="283" y="154.2" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_41_" class="st1"
                                      d="M285.9,165.5c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S290.6,165.5,285.9,165.5z M285.9,150.5c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S289.5,150.5,285.9,150.5z"/>
                           
                            <title>Град Плевен</title></g>
                        <g id="Видин_1_" class="tooltiped city-selector" onclick="changecity(this, 4940,4941)">
                            <circle class="st0" cx="86.8" cy="67.3" r="8.5"/>
                           
                                <rect id="XMLID_39_" x="83.8" y="64.4" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_38_" class="st1"
                                      d="M86.8,75.8c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S91.4,75.8,86.8,75.8z      M86.8,60.8c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S90.4,60.8,86.8,60.8z"/>
                           
                            <title>Град Видин</title></g>
                        <g id="Монтана_1_" class="tooltiped city-selector" onclick="changecity(this, 1,993)">
                            <circle class="st0" cx="135.1" cy="154.1" r="8.5"/>
                           
                                <rect id="XMLID_36_" x="132.2" y="151.2" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_35_" class="st1"
                                      d="M135.1,162.6c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     C143.5,158.8,139.7,162.6,135.1,162.6z M135.1,147.6c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5     C141.6,150.6,138.7,147.6,135.1,147.6z"/>
                           
                            <title>Град Монтана</title></g>
                        <g id="Враца_1_" class="tooltiped city-selector" onclick="changecity(this, 5080,5081)">
                            <circle class="st0" cx="171.5" cy="185.8" r="8.5"/>
                           
                                <rect id="XMLID_33_" x="168.6" y="182.9" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_32_" class="st1"
                                      d="M171.5,194.3c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     C179.9,190.5,176.1,194.3,171.5,194.3z M171.5,179.3c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5     S175.1,179.3,171.5,179.3z"/>
                           
                            <title>Град Враца</title></g>
                        <g id="Перник_1_" class="tooltiped city-selector" onclick="changecity(this, 2706,2707)">
                            <circle class="st0" cx="112.6" cy="273.2" r="8.5"/>
                           
                                <rect id="XMLID_30_" x="109.7" y="270.4" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_29_" class="st1"
                                      d="M112.6,281.7c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S117.3,281.7,112.6,281.7z M112.6,266.7c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S116.2,266.7,112.6,266.7z"/>
                           
                            <title>Град Перник</title></g>
                        <g id="Кюстендил_1_" class="tooltiped city-selector" onclick="changecity(this, 2291,2292)">
                            <circle class="st0" cx="73" cy="319.3" r="8.5"/>
                           
                                <rect id="XMLID_27_" x="70.1" y="316.5" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_26_" class="st1"
                                      d="M73,327.8c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S77.6,327.8,73,327.8z      M73,312.8c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5C79.5,315.8,76.6,312.8,73,312.8z"/>
                           
                            <title>Град Кюстендил</title></g>
                        <g id="Благоевград_1_" class="tooltiped city-selector" onclick="changecity(this, 9,990)">
                            <circle class="st0" cx="116.5" cy="369.6" r="8.5"/>
                           
                                <rect id="XMLID_24_" x="113.6" y="366.7" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_23_" class="st1"
                                      d="M116.5,378.1c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S121.2,378.1,116.5,378.1z M116.5,363.1c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5     C123.1,366,120.2,363.1,116.5,363.1z"/>
                           
                            <title>Град Благоевград</title></g>
                        <g id="Смолян_1_" class="tooltiped city-selector" onclick="changecity(this, 3786,3787)">
                            <circle class="st0" cx="291.4" cy="425.3" r="8.5"/>
                           
                                <rect id="XMLID_21_" x="288.5" y="422.4" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_20_" class="st1"
                                      d="M291.5,433.8c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S296.1,433.8,291.5,433.8z M291.5,418.8c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S295.1,418.8,291.5,418.8z"/>
                           
                            <title>Град Смолян</title></g>
                        <g id="Кърджали_1_" class="tooltiped city-selector" onclick="changecity(this, 1822,1823)">
                            <circle class="st0" cx="366.6" cy="416.8" r="8.5"/>
                            
                                <rect id="XMLID_18_" x="363.7" y="413.9" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_17_" class="st1"
                                      d="M366.7,425.3c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S371.3,425.3,366.7,425.3z M366.7,410.3c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S370.3,410.3,366.7,410.3z"/>
                            
                            <title>Град Кърджали</title></g>
                        <g id="Хасково_1_" class="tooltiped city-selector" onclick="changecity(this, 1560,1561)">
                            <circle class="st0" cx="387.1" cy="373.7" r="8.5"/>
                            
                                <rect id="XMLID_15_" x="384.2" y="370.8" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_14_" class="st1"
                                      d="M387.1,382.2c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S391.8,382.2,387.1,382.2z M387.1,367.2c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5     C393.7,370.1,390.7,367.2,387.1,367.2z"/>
                            
                            <title>Град Хасково</title></g>
                        <g id="Разград_1_" class="tooltiped city-selector" onclick="changecity(this, 3216,3217)">
                            <circle class="st0" cx="489.1" cy="138.4" r="8.5"/>
                            
                                <rect id="XMLID_12_" x="486.1" y="135.5" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_11_" class="st1"
                                      d="M489.1,146.9c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     C497.5,143.1,493.7,146.9,489.1,146.9z M489.1,131.9c-3.6,0-6.5,2.9-6.5,6.5c0,3.6,2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5     S492.7,131.9,489.1,131.9z"/>
                            
                            <title>Град Разград</title></g>
                        <g id="Добрич_1_" class="tooltiped city-selector" onclick="changecity(this, 994,995)">
                            <circle class="st0" cx="627.5" cy="130.2" r="8.5"/>
                            
                                <rect id="XMLID_9_" x="624.6" y="127.3" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_8_" class="st1"
                                      d="M627.5,138.7c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5     S632.2,138.7,627.5,138.7z M627.5,123.7c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S631.1,123.7,627.5,123.7z"/>
                            
                            <title>Град Добрич</title></g>
                        <g id="Русе_1_" class="tooltiped city-selector" onclick="changecity(this, 3320,3321)">
                            <circle class="st0" cx="433" cy="98.8" r="8.6"/>
                            
                                <rect id="XMLID_6_" x="430.1" y="95.9" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_5_" class="st1"
                                      d="M433,107.3c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S437.7,107.3,433,107.3z      M433,92.3c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5C439.6,95.2,436.6,92.3,433,92.3z"/>
                            
                            <title>Град Русе</title></g>
                        <g id="Силистра_1_" class="tooltiped city-selector" onclick="changecity(this, 3555,3556)">
                            <circle class="st0" cx="557.9" cy="63.9" r="8.5"/>
                            
                                <rect id="XMLID_3_" x="555" y="61" class="st1" width="5.8" height="5.8"/>
                                <path id="XMLID_2_" class="st1"
                                      d="M557.9,72.4c-4.7,0-8.5-3.8-8.5-8.5s3.8-8.5,8.5-8.5s8.5,3.8,8.5,8.5S562.6,72.4,557.9,72.4z      M557.9,57.4c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5S561.5,57.4,557.9,57.4z"/>
                            
                            <title>Град Силистра</title></g>
                   ';

		$d = new \DOMDocument;
		libxml_use_internal_errors(true);
		$d->loadHTML('<?xml encoding="utf-8" ?>' . $svg);
		libxml_clear_errors();
		$cities = [];

		$skipAttributes = ['id', 'onclick'];
		$skipNodes = ['title'];

		foreach ($d->getElementsByTagName('g') as $element) {
			if ($element->hasAttributes()) {
				$cityName = str_replace('_1_', '', $element->getAttribute('id'));
				$cities[$cityName] = [];

				if ($element->hasChildNodes()) {
					foreach ($element->childNodes as $c) {
						$childAttr = [];
						if ($c->nodeType == XML_ELEMENT_NODE) {

							if (!in_array(strtolower($c->tagName), $skipNodes)) {
								foreach ($c->attributes as $attr) {
									$name = $attr->nodeName;
									$value = $attr->nodeValue;

									if (!in_array(strtolower($name), $skipAttributes)) {
										$childAttr[$name] = preg_replace('!\s+!', ' ', $value);
									}
								}

								$cities[$cityName][$c->tagName][] = $childAttr;
							}
						}
					}
				}
			}
			//dd('lol');
		}

		/*dd(count($cities));
		dd($cities);*/

		foreach ($cities as $city => $elements) {
			$area = Area::where('name', 'LIKE', '%' . $city . '%')
			            ->first();
			if ($area) {

				$city = City::create([
					'uuid'              => getUUID(),
					'area_id'           => $area->id,
					'name'              => $city,
					'is_area_main_city' => 1,
					'country_map'       => json_encode($elements),
				]);
			}
		}
	}
}