<?php

namespace App\Http\Middleware;

use Closure;

class Locale {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle ($request, Closure $next) {

		$lang = env('DEFAULT_LOCALE', 'bg');

		if ($request->session()
		            ->has('lang')) {
			$lang = strtolower($request->session()
			                           ->get('lang'));
		} else {
			$request->session()
			        ->put('lang', $lang);
		}

		\App::setLocale($lang);

		return $next($request);
	}
}
