<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use ReflectionClass;

abstract class Job {

	/*
	|--------------------------------------------------------------------------
	| Queueable Jobs
	|--------------------------------------------------------------------------
	|
	| This job base class provides a central location to place any logic that
	| is shared across all of your jobs. The trait included with the class
	| provides access to the "onQueue" and "delay" queue helper methods.
	|
	*/

	use Queueable;

	public function __getIdentifier () {
		$reflectionClass = new ReflectionClass(get_called_class());

		return ' [' . $reflectionClass->getShortName() . '] [' . $reflectionClass->getName() . '] [' . $reflectionClass->getFileName() . '] ';
	}
}
