<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Request;

class ViewServiceProvider extends ServiceProvider {

	/**
	 * Register bindings in the container.
	 *
	 * @return void
	 */
	public function boot () {
		view()->composer(['frontend::*', '*'], 'App\Composers\ViewComposer');

		// Attach the Zombies, Gateways, Filters, Rules .... everywhere

		/*view()->composer(['templates::*', 'whitelisting::*'], function ($view) {

		});*/
		//$view->with('request', $request);

	}

	/**
	 * Register
	 *
	 * @return void
	 */
	public function register () {
		//
	}
}