<?php

namespace App\Providers;

use Auth;
use Debugbar;
use Helpers\UUID;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Nwidart\Modules\Module;
use Route;

class ModelServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot () {

		// Events Needs remapping with correct values

		$events = [
			'saving',
			'deleting',
			'updating',
			'creating',
		];

		Debugbar::startMeasure('eloquent.events');

		// Modules should be fetched based on the vertical
		/*foreach (Module::enabled() as $module) {
			foreach ($events as $map => $event) {
				if (method_exists($this, '_'.$event)) {
					$this->{'_'.$event};
				}
			}
		}*/
		Debugbar::stopMeasure('eloquent.events');
	}

	public function _saving () {
		Debugbar::startMeasure('log.events');



		Debugbar::stopMeasure('log.events');
	}

	private function _creating ($modelToAttach) {
		Debugbar::startMeasure('log.events._creating');

		$modelToAttach::creating(function ($model) {

			if (!isset($model->uuid) || trim($model->uuid) == '' || is_null($model->uuid)) {
				$model->uuid = (string) UUID::generate(1);

				return true;
			}
		});

		Debugbar::stopMeasure('log.events._creating');
	}

	private function _updating ($modelToAttach) {
		Debugbar::startMeasure('log.events._updating');

		$modelToAttach::creating(function ($model) {
			return true;
		});

		Debugbar::stopMeasure('log.events._updating');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register () {
		//
	}
}
