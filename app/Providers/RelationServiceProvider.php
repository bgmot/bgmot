<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Modules\Campaign\Entities\Campaign;
use Modules\Campaign\Entities\CampaignData;
use Modules\Campaign\Entities\CampaignHeaders;
use Modules\Campaign\Entities\CampaignLinks;
use Modules\Campaign\Entities\CampaignLog;
use Modules\Campaign\Entities\CampaignReadyData;
use Modules\Campaign\Entities\CampaignSegment;
use Modules\Campaign\Entities\CampaignServers;
use Modules\Campaign\Entities\CampaignService;
use Modules\Campaign\Entities\CampaignStatistics;
use Modules\Campaign\Entities\CampaignTemplate;
use Modules\Campaign\Entities\CampaignTemplateRevision;
use Modules\Campaign\Entities\CampaignTemplateRevisionSettings;
use Modules\Campaign\Entities\CampaignTemplateSettings;
use Modules\Campaign\Entities\CampaignTracking;
use Modules\Campaign\Entities\CampaignType;
use Modules\Campaign\Entities\CampaignVersion;
use Modules\Customer\Entities\CustomerGroup;
use Modules\Customer\Entities\CustomerRole;

class RelationServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot () {

		// Should be loaded from Database
		Relation::morphMap([
			// Customer Part
			'group' => CustomerGroup::class,
			'role'  => CustomerRole::class,
		]);
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register () {
		//
	}
}
