<?php

namespace App\Providers;

use Cache;
use Auth;
use Config;
use Doctrine\DBAL\Exception\TableNotFoundException;
use Helpers\Common;
use Helpers\Helper;
use Helpers\UUID;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Nwidart\Modules\Facades\Module;
use Route;

class ModuleServiceProvider extends ServiceProvider {

	private $_routeMethods = [
		'get',
		'post',
		'put',
		'delete',
		'patch',
		'any',
	];

	private $_middlewares = [
		'web',
		'auth',
		'access',
	];

	private $_loadedModules = [];

	private $_loadedControllers = [];

	private $_loadedMethods = [];

	private $_loadedRoutes = [];

	public function boot () {
		//return $this;

		$defaultTemplatePath = new \stdClass();
		$defaultTemplatePath->path = Config::get('platform.options.global.backend-template');

		$siteTemplate = $defaultTemplatePath;

		$activeTemplate = public_path() . '/_templates/main';
		$templateType = 'Main';

		$modules = [];

		foreach (Module::all() as $module) {
			$controllers = $methods = $routes = [];

			if (!in_array($module->name, $this->_loadedModules)) {

				// load custom routes (hardcoded) if any, others will be pulled from database
				if (file_exists(base_path() . '/modules/' . $module->name . '/Http/routes.php')) {
					include base_path() . '/modules/' . $module->name . '/Http/routes.php';
				}

				if (is_dir($activeTemplate . '/_views/' . strtolower($module->name))) {
					$this->loadViewsFrom($activeTemplate . '/_views/' . strtolower($module->name), $module->name);
				}

				$this->_loadedModules[] = $module->name;

				$modules[$module->id] = [
					'module'      => $module,
					'controllers' => $controllers,
					'routes'      => $routes,
				];
			}
		}

		$this->loadViewsFrom($activeTemplate . '/_views/auth', 'Auth');

		// this is the view helpers
		$this->loadViewsFrom($activeTemplate . '/_layouts', $templateType);

		$this->loadViewsFrom($activeTemplate . '/_helpers', 'Helpers');

		/// this is for the errors
		$this->loadViewsFrom($activeTemplate . '/backend/_errors', 'Error');
	}

	public function register () {

	}

	private function loadRoutesForModule ($module = null) {

		if (!is_null($module)) {

			$cacheRoutesTime = Config::get('platform.options.cache.routes');

			if (is_null($cacheRoutesTime)) {
				$cacheRoutesTime = 0;
			}

			$generatedRoutes = [];

			//$ee = $module->entities;

			$mObject = json_decode(json_encode($module));

			$callback = function () use ($module, &$generatedRoutes, $cacheRoutesTime, $mObject) {

				if ($cacheRoutesTime > 0 && Cache::store('redis-local')
				                                 ->tags('modules')
				                                 ->has('module_' . $module->uuid)
				) {
					$routes = json_decode(Cache::store('redis-local')
					                           ->tags('modules')
					                           ->get('module_' . $module->uuid), true);

					foreach ($routes as $route) {
						$routeMethod = $route['method'];
						$routePath = $route['path'];
						$routeParams = json_decode($route['params'], true);
						$routeType = $route['type'];

						if ($routeType != 1) {
							$closure = $route['closure'];
							$routeParams[] = function () use ($closure) {
								return eval($closure);
							};
						}

						Route::$routeMethod(strtolower($routePath), $routeParams);
					}
				} else {
					$generatedRoutes = [];

					//foreach ($this->_loadedRoutes[$module->id] as $routeId => $route) {
					foreach ($module->routes->sortBy('path') as $route) {
						if (isset($this->_loadedControllers[$module->id][$route->controller_id])) {
							$controller = $this->_loadedControllers[$module->id][$route->controller_id];
						} else {
							$controller = $route->controller;
						}

						if (isset($this->_loadedMethods[$module->id][$route->controller_id][$route->method_id])) {
							$method = $this->_loadedMethods[$module->id][$route->controller_id][$route->method_id];
						} else {
							$method = $route->method;
						}

						if ($controller && $method) {

							$middlewares = [];

							if (isset($route->middleware) && $route->middleware != '' && count($route->middleware) > 0) {
								foreach (explode(',', $route->middleware) as $middleware) {
									$middlewares[] = $middleware;
								}
							}

							$routeParams = [];

							$routeParams['as'] = $route->name;
							//$routeParams['as'] = strtolower($module->name . '.' . $controller->name . '.' . $action->name . '.' . $route->name);
							//$routeParams['role'] = $r->role;
							$routeParams['uuid'] = $route->uuid;

							$routeMethod = strtolower($method->http_action);
							//$routePath = $route->path;

							$routePaths = [];
							if (!is_null($controller->path)) {
								$routePaths[] = ltrim($controller->path, '/');
							}

							/*if (!is_null($action->path)) {
								$routePaths[] = ltrim($action->path, '/');
							}*/

							if (!is_null($route->path)) {
								$routePaths[] = ltrim($route->path, '/');
							}

							$routePath = null;
							if (count($routePaths) > 0) {
								$routePath = implode('/', $routePaths);
							}

							if (!in_array($routeMethod, $this->_routeMethods)) {
								$routeMethod = 'get';
							}

							if (is_null($routePath) || trim($routePath) == '') {
								//$routePath = (!is_null($action->path) ? $action->path : '/');

							}

							$routePath = $route->path;

							if ($module->is_enabled != 1) {
								$routeParams = function () {
									d(403, '[M] Unauthorized action.');
									abort(403, '[M] Unauthorized action.');

									return;
								};
							} else {
								$_class = $module->namespace . '\\' . $controller->class;

								$reflection = null;

								if (class_exists($_class)) {
									$reflection = new \ReflectionClass($_class);
								}

								if ($controller->is_enabled != 1) {
									$routeParams = function () {
										d(403, '[C] Unauthorized action.');
										abort(403, '[C] Unauthorized action.');

										return;
									};
								} else if (is_null($reflection)) {
									$routeParams = function () {
										//d(405, '[C] Invalid action.');
										abort(405, '[C] Invalid action.');

										return;
									};
								} else {
									if ($method->is_enabled != 1) {
										$routeParams = function () {
											d(403, '[A] Unauthorized action.');
											abort(403, '[A] Unauthorized action.');

											return;
										};
									} else if (!$reflection->hasMethod($method->name)) {
										$routeParams = function () {
											//d(405, '[M] Invalid action.');
											abort(405, '[M] Invalid action.');

											return;
										};
									} else {
										if ($route->is_enabled != 1) {
											$routeParams = function () {
												d(403, '[R] Unauthorized action.');
												abort(403, '[R] Unauthorized action.');

												return;
											};
										} else {
											if ($route->route_type == 1) {
												$routeParams['uses'] = $controller->class . '@' . $method->name;
											} else {
												if (!is_null($route->closure)) {
													$closure = $route->closure;
													$routeParams[] = function () use ($closure) {
														return eval($closure);
													};
												}
											}
										}
									}
								}
							}

							if (is_array($routeParams)) {
								if (count($middlewares) > 0 || count($this->_middlewares) > 0) {
									$routeParams['middleware'] = array_merge($this->_middlewares, $middlewares);

									if ($module->name == 'Auth') {
										foreach (['auth', 'access'] as $middleware) {
											if (in_array($middleware, $routeParams['middleware'])) {
												$key = array_search($middleware, $routeParams['middleware']);
												if ($key && isset($routeParams['middleware'][$key])) {
													unset($routeParams['middleware'][$key]);
												}
											}
										}
									}
								}
							}

							if (is_null($routePath)) {
								$routePath = Common::decamelize(strtolower(str_replace('Controller', '', $controller->name))) . '/' . UUID::generate(1);
							}

							Route::$routeMethod(strtolower($routePath), $routeParams);

							$generatedRoutes[] = [
								'path'    => strtolower($routePath),
								'method'  => $routeMethod,
								'type'    => $route->route_type,
								'params'  => ($route->route_type != 1) ? '[]' : json_encode($routeParams),
								'closure' => ($route->route_type != 1) ? $route->closure : null,
							];

							// we cache each route separately
							if ($cacheRoutesTime > 0) {
								\Debugbar::startMeasure('cache_' . $module->id);
								Cache::store('redis-local')
								     ->tags('routes')
								     ->put($route->name, json_encode([
									     'module'          => $module->name,
									     'module_id'       => $module->id,
									     'module_uuid'     => $module->uuid,
									     'controller_id'   => $controller->id,
									     'controller_uuid' => $controller->id,
									     'method_id'       => $method->id,
									     'method_uuid'     => $method->id,
									     'route_id'        => $route->id,
									     'route_uuid'      => $route->uuid,
									     /*'module_object'   => $module->toJson(),
									     'route_object'    => $route->toJson(),*/

									     'module_object' => $mObject,
									     //'route_object'    => json_encode($route),

								     ]), Config::get('platform.options.cache.routes'));
								\Debugbar::stopMeasure('cache_' . $module->id);
							}
						}
					}

					if ($cacheRoutesTime > 0) {
						Cache::store('redis-local')
						     ->tags('modules')
						     ->put('module_' . $module->uuid, json_encode($generatedRoutes), Config::get('platform.options.cache.routes'));

						Cache::store('redis-local')
						     ->tags('modules_assets')
						     ->put('module_assets_' . $module->uuid, $module->asset->toJson(), Config::get('platform.options.cache.routes'));
					}
				}
			};

			// we generate the SMS routes

			if ($module->name != 'Auth') {


				Route::group([
					'prefix'    => 'sms/' . $module->prefix,
					'module'    => $module->name,
					'namespace' => $module->namespace,
					'domain'    => Helper::getCustomerSlug(),
				], $callback);

				// This is 2nd group without the domain so we allow CNAMEs
				Route::group([
					'prefix'    => 'sms/' . $module->prefix,
					'module'    => $module->name,
					'namespace' => $module->namespace,
				], $callback);

				// we generate the Email routes
				Route::group([
					'prefix'    => 'email/' . $module->prefix,
					'module'    => $module->name,
					'namespace' => $module->namespace,
					'domain'    => Helper::getCustomerSlug(),
				], $callback);

				// This is 2nd group without the domain so we allow CNAMEs
				Route::group([
					'prefix'    => 'email/' . $module->prefix,
					'module'    => $module->name,
					'namespace' => $module->namespace,
				], $callback);
			} else {
				Route::group([
					'prefix'    => $module->prefix,
					'module'    => $module->name,
					'namespace' => $module->namespace,
					'domain'    => Helper::getCustomerSlug(),
				], $callback);

				// This is 2nd group without the domain so we allow CNAMEs
				Route::group([
					'prefix'    => $module->prefix,
					'module'    => $module->name,
					'namespace' => $module->namespace,
				], $callback);
			}
		}

		return true;
	}
}
