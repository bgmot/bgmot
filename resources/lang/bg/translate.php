<?php

return [
	'routes' => [
		'search'  => 'търсене',
		'agency'  => 'агенции',
		'brokers' => 'брокери',
	],

	'name'              => 'Име',
	'search'            => 'Търсене',
	'login'             => 'Вход',
	'search'            => 'Търсене',
	'agency'            => 'Агенции',
	'brokers'           => 'Брокери',
	'property_looking'  => 'Търсещи имот',
	'property_offering' => 'Предлагащи имот',
	'register'          => [
		'title'  => 'Регистрация',
		'select' => [
			'user'   => 'User',
			'seek'   => 'Търся имот',
			'agency' => 'Агенция/фирма',
			'broker' => 'Брокер',
			'owner'  => 'Собственик на имот',
		],
		'form'   => [
			'firstname'             => 'Име',
			'lastname'              => 'Фамилия',
			'email'                 => 'E-mail адрес',
			'phone'                 => 'Мобилен',
			'password'              => 'Парола',
			'password_confirmation' => 'Повтори парола',
			'button'                => 'Регистрация',
			'agreement'             => 'Съгласен съм с общите условия на ' . env('APP_DOMAIN'),
			'area'                  => 'Област',
			'city'                  => 'Град',
			'neighbor'              => 'Квартал',
			'price'                 => 'Цена',
			'floor'                 => 'Етаж',
			'property_type'         => 'Вид имот',
			'property_settings'     => 'Характеристики',
			'ad_type'               => 'Наем, продажба, замяна',
			'user_sub_type'         => '?',
			'user_sub'              => [
				'seek'  => 'Търся имот',
				'owner' => 'Собственик на имот',
			],
			'agency'                => [
				'name'               => 'Пълно име на фирмата',
				'address'            => 'Адресна регистрация',
				'id_number'          => 'БУЛСТАТ',
				'tax_number'         => 'Данъчен номер',
				'responsible_person' => 'МОЛ',
				'contact_person'     => 'Лице за кореспонденция на фирмата',
				'phone'              => 'Телефон за контакт на титуляра',
				'public_data'        => 'Видими данни за вашия профил',
				'public_name'        => 'Име на агенцията',
				'public_address'     => 'Офис Адрес',
				'public_area'        => 'Област',
				'public_city'        => 'Град',
				'public_phone'       => 'Мобилен',
				'public_web'         => 'Web Сайт',

			],
			'broker'                => [

			],
			'user'                  => [

			],
		],
	],
	'login'             => [
		'title' => 'Вход',
		'form'  => [
			'email'       => 'E-mail адрес',
			'password'    => 'Парола',
			'remember_me' => 'Remember me',
			'button'      => 'Вход',
		],
	],

	'property' => [
		'sale'         => 'Продажба',
		'rent'         => 'Наем',
		'trade'        => 'Замяна',
		'roommate'     => 'Съквартирант',
		'construction' => 'В строеж',
	],

	'attributes' => [

	],
];