<?php

return [
	'routes'            => [
		'search'  => 'search',
		'agency'  => 'agency',
		'brokers' => 'brokers',
	],
	'name'              => 'Name',
	'search'            => 'Search',
	'login'             => 'Login',
	'search'            => 'Search',
	'agency'            => 'Agencies',
	'brokers'           => 'Brokers',
	'property_looking'  => 'Looking property',
	'property_offering' => 'Offering property',
	'register'          => [
		'title'  => 'Register',
		'select' => [
			'user'   => 'Корисник',
			'seek'   => 'Looking for property',
			'agency' => 'Agency/Company',
			'broker' => 'Broker',
			'owner'  => 'Property Owner',
		],
		'form'   => [
			'firstname'             => 'First name',
			'lastname'              => 'Last name',
			'email'                 => 'E-mail address',
			'phone'                 => 'Phone',
			'password'              => 'Password',
			'password_confirmation' => 'Password Confirmation',
			'button'                => 'Register',
			'agreement'             => 'I agree to terms and conditions of ' . env('APP_DOMAIN'),
			'area'                  => 'Area',
			'city'                  => 'City',
			'neighbor'              => 'Neighbor',
			'price'                 => 'Price',
			'floor'                 => 'Floor',
			'agency'                => [
				'name'               => 'Full agency name',
				'address'            => 'Адресна регистрация',
				'id_number'          => 'ID Number',
				'tax_number'         => 'Tax number',
				'responsible_person' => 'Responsible person',
				'contact_person'     => 'Agency contact person',
				'phone'              => 'Agency phone',
				'public_data'        => 'Public visible data for the agency',
				'public_name'        => 'Agency name',
				'public_address'     => 'Agency address',
				'public_area'        => 'Area',
				'public_city'        => 'City',
				'public_phone'       => 'Phone',
				'public_web'         => 'Web site',

			],
			'broker'                => [

			],
			'user'                  => [

			],
		],
	],
	'login'             => [
		'title' => 'Please Sign In',
		'form'  => [
			'email'       => 'Email',
			'password'    => 'Password',
			'remember_me' => 'Remember me',
			'button'      => 'Login',
		],
	],
	'property'          => [
		'sale'         => 'Sale',
		'rent'         => 'Rent',
		'trade'        => 'Trade',
		'roommate'     => 'Roommate',
		'construction' => 'Construction',
	],
];