<?php

namespace Modules\Auth\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Ad\Entities\Ads;

class RegisterRequest extends FormRequest {

	public $currentRequest;

	public function __construct () {
		$this->currentRequest = \Route::getCurrentRequest();

		parent::__construct(func_get_args());
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize () {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules () {
		return [
			'name'     => 'required|string|max:255',
			'email'    => 'required|string|email|max:255|unique:user',
			'password' => 'required|string|min:6|confirmed',
			'area'     => 'required',
			'city'     => 'required',
		];
	}

	public function messages () {
		return [
			'name.required' => 'A name is required',
			'name.max'      => 'The field has to be max :max chars long',

		];
	}

	public function withValidator ($validator) {
		if ($validator->fails()) {
			$validator->after(function ($validator) {
				$this->currentRequest->session()
				                     ->flash('ad', $this->get('ad-params'));
			});
		}
	}
}
