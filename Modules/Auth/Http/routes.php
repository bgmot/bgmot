<?php

Route::group([
	'middleware' => ['web', 'guest'],
	'prefix'     => 'auth',
	'namespace'  => 'Modules\Auth\Http\Controllers',
], function () {
	//Route::get('/', 'AuthController@index');

	Route::get('login', [
		'uses' => 'AuthController@showLoginForm',
		'as'   => 'auth.getLogin',
	]);

	Route::post('login', [
		'uses' => 'AuthController@login',
		'as'   => 'auth.postLogin',
	]);

	Route::post('logout', [
		'uses' => 'AuthController@logout',
		'as'   => 'auth.postLogout',
	]);

	Route::get('register', [
		'uses' => 'RegisterController@showRegistrationForm',
		'as'   => 'auth.getRegister',
	]);

	Route::post('register', [
		'uses' => 'RegisterController@register',
		'as'   => 'auth.postRegister',
	]);

	Route::get('password/reset', [
		'uses' => 'ForgotPasswordController@showLinkRequestForm',
		'as'   => 'auth.getForgotPassword',
	]);

	Route::post('password/reset', [
		'uses' => 'ResetPasswordController@reset',
		'as'   => 'auth.postForgotPassword',
	]);

	Route::post('password/email', [
		'uses' => 'ForgotPasswordController@sendResetLinkEmail',
		'as'   => 'auth.postForgotPasswordEmail',
	]);

	Route::get('password/reset/{token}', [
		'uses' => 'ResetPasswordController@showResetForm',
		'as'   => 'auth.getForgotPasswordForm',
	]);

	Route::post('/ajax/check-login-info', [
		'uses' => 'AjaxController@checkLoginInfo',
		'as'   => 'auth.check-login-info',
	]);

	Route::post('/ajax/load-register-form', [
		'uses' => 'AjaxController@loadRegisterForm',
		'as'   => 'auth.load-register-form',
	]);

	Route::post('/ajax/register-user', [
		'uses' => 'AjaxController@registerUser',
		'as'   => 'auth.register-user',
	]);
});

Route::get('/auth/logout', [
	'middleware' => 'web',
	'uses'       => 'Modules\Auth\Http\Controllers\AuthController@getLogout',
	'as'         => 'auth.getLogout',
]);


