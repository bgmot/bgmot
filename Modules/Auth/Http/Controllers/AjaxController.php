<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\UserRegistered;
use Auth;
use Config;
use DB;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Modules\Ad\Entities\AdsType;
use Modules\Ad\Entities\Attributes;
use Modules\Ad\Entities\AttributeType;
use Modules\Agency\Entities\Agency;
use Modules\User\Entities\User;
use Modules\User\Entities\UserType;
use View;
use Validator;

class AjaxController extends Controller {

	/**
	 * AjaxController constructor.
	 * @param Request $request
	 */
	public function __construct (Request $request) {

		if (!$request->ajax() || !$request->wantsJson()) {
			return response()->json([
				'message' => 'Unauthorized',
				'error'   => 1000,
			], 400);
		}
	}

	public function checkLoginInfo (Request $request) {
		$isOk = false;
		$user = User::where('email', $request->get('email'))
			//->where('password', \Hash::make($request->get('password')))
			        ->first();

		if ($user && Hash::check($request->get('password'), $user->password)) {
			$isOk = true;
		}

		if ($isOk) {

			Auth::attempt([
				'email'    => $request->get('email'),
				'password' => $request->get('password'),
			]);

			return response()->json([
				'error' => [
					'description' => '',
					'code'        => 0,
				],
				'data'  => [
					'message' => 'Success',
				],
			], 200);
		}

		return response()->json([
			'error' => [
				'description' => 'Wrong credentials',
				'code'        => 1000,
			],
		], 400);
	}

	public function loadRegisterForm (Request $request) {
		$userType = UserType::where('id', $request->get('user_type'))
		                    ->where('is_enabled', 1)
		                    ->where('is_administrator', 0)
		                    ->first();

		if ($userType) {

			return response()->json([
				'error' => [
					'description' => '',
					'code'        => 0,
				],
				'data'  => [
					'html' => View::make('Auth::register.ajax.' . $userType->name, [
						'adsType'                => AdsType::where('is_enabled', 1)
						                                   ->get(),
						'attributeTypes'         => AttributeType::where('is_enabled', 1)
						                                         ->where('is_register', 1)
						                                         ->get(),
						'currencyAttributeTypes' => AttributeType::where('is_enabled', 1)
						                                         ->where('name', 'currency')
						                                         ->get(),
					])
					              ->render(),
				],
			], 200);
		}

		return response()->json([
			'error' => [
				'description' => 'Invalid UserType',
				'code'        => 1000,
			],
		], 400);
	}

	public function registerUser (Request $request) {
		$data = [];

		parse_str($request->get('data'), $data);

		$userType = UserType::where('id', $data['user_type'])
		                    ->where('is_administrator', 0)
		                    ->first();

		if ($userType) {
			$rules = array_merge(Config::get('platform.validate.default.rules'), Config::get('platform.validate.' . strtolower($userType->name) . '.rules'));
			$messages = array_merge(Config::get('platform.validate.default.messages'), Config::get('platform.validate.' . strtolower($userType->name) . '.messages'));

			$validator = Validator::make($data, $rules, $messages);

			$errorCode = 0;
			$errorDescription = '';

			$messages = []; // this will hold the error messages
			if ($validator->passes()) {

				$status = false;
				$user = null;
				try {
					DB::transaction(function () use (&$status, &$user, $data, $userType) {
						// register the user

						$user = User::create([
							'uuid'            => getUUID(),
							'email'           => $data['email'],
							'password'        => Hash::make($data['password']),
							'user_type_id'    => $data['user_type'],
							'activation_code' => getRandomUUID(),
							'activated_at'    => null,
						]);

						// create the user details
						$user->detail()
						     ->create([
							     'uuid'      => getUUID(),
							     'firstname' => $data['firstname'],
							     'lastname'  => $data['lastname'],
							     'address'   => array_key_exists('address', $data) ? $data['address'] : null,
							     'phone'     => array_key_exists('phone', $data) ? $data['phone'] : null,
							     'web'       => array_key_exists('web', $data) ? $data['web'] : null,
							     //'country_id'         => $data[''], // ?
							     'area_id'   => $data['area_id'],
							     'city_id'   => $data['city_id'],
						     ]);

						if ($userType->name == 'agency') {
							// create agency profile
							$agency = Agency::create([
								'uuid'               => getUUID(),
								'name'               => $data['name'],
								'id_number'          => $data['id_number'],
								'tax_number'         => $data['tax_number'],
								'responsible_person' => $data['responsible_person'],
								'contact_person'     => $data['contact_person'],
								'address'            => $data['address'],
								'phone'              => $data['phone'],
								'main_user_id'       => $user->id,
								//'country_id'         => $data[''], // ?
								'area_id'            => $data['area_id'],
								'city_id'            => $data['city_id'],
							]);

							$agency->detail()
							       ->create([
								       'uuid'    => getUUID(),
								       'name'    => ($data['public_name'] != '') ? $data['public_name'] : $agency->name,
								       'address' => ($data['public_address'] != '') ? $data['public_address'] : $agency->address,
								       'phone'   => ($data['public_phone'] != '') ? $data['public_phone'] : $agency->phone,
								       'web'     => ($data['public_web'] != '') ? $data['public_web'] : $agency->web,
								       //'country_id'         => $data[''], // ?
								       'area_id' => ($data['public_area_id'] != '') ? $data['public_area_id'] : $agency->area_id,
								       'city_id' => ($data['public_city_id'] != '') ? $data['public_city_id'] : $agency->city_id,
							       ]);
						}

						if ($userType->name == 'broker') {
							// look for agency based on the data
							$agency = Agency::where('id_number', $data['id_number'])
							                ->first();

							if ($agency) {
								$agency->broker()
								       ->create([
									       'uuid' => getUUID(),
								       ]);
							}
						}

						if ($userType->name == 'user') {
							// insert the asked/offering property in the ads table

							$ad = $user->ads()
							           ->create([
								           'uuid'        => getUUID(),
								           'user_sub_id' => $data['user_sub_id'],
								           'name'        => null,
								           'description' => null,
								           'is_initial'  => 1,
								           'main_image'  => null,
								           'ad_type_id'  => $data['ad_type_id'],
								           //'country_id'         => $data[''], // ?
								           'area_id'     => $data['area_id'],
								           'city_id'     => $data['city_id'],
								           'neighbor_id' => $data['neighbor_id'],
								           'price'       => $data['price'],
								           'old_price'   => $data['price'],
							           ]);

							foreach ($data['attribute'] as $attributeId => $attributeValue) {
								$ad->attributes()
								   ->create([
									   'attribute_id' => $attributeId,
									   'value'        => $attributeValue,
								   ]);
							}

							if (array_key_exists('attachment', $data)) {
								foreach ($data['attachment'] as $attachment) {
									$ad->attachments()
									   ->create([
										   'filename' => $attachment['filename'],
										   'path'     => $attachment['path'],
										   'mime'     => $attachment['mime'],
									   ]);
								}
							}
						}

						$status = true;
					}, 3);
				} catch (\Exception $e) {
					dd($e->getMessage());
					//dd($e->getTraceAsString());
				} catch (\Throwable $e) {
					dd($e->getMessage());
					//dd($e->getTraceAsString());
				}

				if ($status) {
					$message = (new UserRegistered($user))->onConnection('redis')
					                                      ->onQueue('registration');

					Mail::to($user)
					    ->bcc(env('DEFAULT_BCC'))
					    ->queue($message);
				}
			} else {
				$errorCode = 1000;
				$errorDescription = 'Registration failed';

				foreach ($validator->errors()
				                   ->messages() as $field => $message) {
					foreach ($message as $msg) {
						$messages[$field][] = $msg;
					}
				}
			}

			return response()->json([
				'error' => [
					'description' => $errorDescription,
					'code'        => $errorCode,
				],
				'data'  => [
					'messages' => $messages,
				],
			], 200);
		}
	}

}
