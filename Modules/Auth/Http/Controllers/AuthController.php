<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use View;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	public $redirectAfterLogout = '/';
	public $lockoutTime = 60;
	public $maxLoginAttempts = 5;
	public $loginView = 'Auth::auth.login';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct () {
		$this->middleware('guest', [
			'except' => [
				'getLogout',
				'logout',
			],
		]);
	}

	public function showLoginForm () {
		return View::make('Auth::auth.login');
	}

	public function index () {
		return redirect($this->loginPath());
	}

	public function loginPath () {
		return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
	}

	protected function credentials (Request $request) {
		$extraCredentialData = [
			'is_blocked' => 0,
			'is_active'  => 1,
		];

		return array_merge($request->only($this->username(), 'password'), $extraCredentialData);
	}

	public function getLogout (Request $request) {
		return $this->logout($request);
	}

	public function logout (Request $request) {
		$this->guard()
		     ->logout();

		$request->session()
		        ->invalidate();

		return redirect('/');
	}

	public function login (Request $request) {
		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if ($this->hasTooManyLoginAttempts($request)) {
			$this->fireLockoutEvent($request);

			return $this->sendLockoutResponse($request);
		}

		if ($this->attemptLogin($request)) {
			return $this->sendLoginResponse($request);
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}

	protected function sendLoginResponse (Request $request) {
		$request->session()
		        ->regenerate();

		$this->clearLoginAttempts($request);

		return $this->authenticated($request, $this->guard()
		                                           ->user())
			?: redirect()->intended($this->redirectPath());
	}

	protected function sendFailedLoginResponse (Request $request) {
		$errors = [$this->username() => 'Login failed'];

		if ($request->expectsJson()) {
			return response()->json($errors, 422);
		}

		return redirect()
			->back()
			->withInput($request->only($this->username(), 'remember'))
			->withErrors($errors);
	}

}
