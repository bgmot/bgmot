<?php

namespace Modules\Search\Entities;

use Illuminate\Database\Eloquent\Model;

class SavedSearch extends Model
{
    protected $fillable = [];
}
