<?php
$callback = function () {
	Route::match(['GET', 'POST'], '/{deal_type?}', [
		'uses' => 'SearchController@index',
		'as'   => 'search.search',
	]);
};

/*Route::group([
	'middleware' => 'web',
	'prefix'     => 'търсене',
	'namespace'  => 'Modules\Search\Http\Controllers',
], $callback);*/

Route::group([
	'middleware' => 'web',
	'prefix'     => 'search',
	'namespace'  => 'Modules\Search\Http\Controllers',
], $callback);
