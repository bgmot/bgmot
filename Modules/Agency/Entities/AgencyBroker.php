<?php

namespace Modules\Agency\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class AgencyBroker extends Model {

	protected $table = 'agency_broker';
	protected $guarded = [];

	public function agency () {
		return $this->belongsTo(Agency::class);
	}
}
