<?php

namespace Modules\Agency\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class AgencyDetail extends Model {

	protected $table = 'agency_detail';
	protected $guarded = [];

	public function agency () {
		return $this->belongsTo(Agency::class);
	}
}
