<?php

namespace Modules\Agency\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class Agency extends Model {

	protected $table = 'agency';
	protected $guarded = [];

	public function brokers () {
		return $this->hasManyThrough(User::class);
	}

	public function detail () {
		return $this->hasOne(AgencyDetail::class);
	}

	public function offices () {
		return $this->hasMany(AgencyOffice::class);
	}
}
