<?php

namespace Modules\Agency\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class AgencyOffice extends Model {

	protected $table = 'agency_office';
	protected $guarded = [];

	public function agency () {
		return $this->belongsTo(Agency::class);
	}
}
