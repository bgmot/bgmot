<?php

namespace Modules\Agency\Providers;

use Helpers\Validate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Modules\Agency\Entities\Agency;

class RouteServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;
	protected $namespace = 'Modules\Agency\Http\Controllers';
	protected $module = 'Agency';

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */

	public function boot () {

		$relations = [
			'agency' => ['class' => Agency::class, 'field' => 'uuid'],
		];

		$router = $this->app['router'];

		foreach ($relations as $model => $data) {
			$router->bind($model, function ($value) use ($model, $data, $router) {

				$check = Validate::is_uuid($value);

				if ($check) {
					$class = $data['class'];

					$object = null;
					//\DB::enableQueryLog();
					try {
						$object = $class::where($data['field'], $value)
						                ->first();
					} catch (ModelNotFoundException $e) {
						dd(__FILE__, 'aa');
					} catch (\Exception $e) {
						dd(__FILE__, $e->getMessage());
					}

					//dd(\DB::getQueryLog());
					if (!is_null($object)) {
						return $object;
					}
				}

				return false;
			});
		}

		parent::boot();
	}

	public function register () {
	}

	public function map () {
		/*$router = $this->app['router'];

		$router->group(['namespace' => $this->namespace], function ($router) {

			$file = base_path('modules/' . $this->module . '/Http/routes.php');
			if (file_exists($file)) {
				require $file;
			}
		});*/
	}

}
