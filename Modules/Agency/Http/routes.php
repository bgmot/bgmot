<?php

Route::group([
	'middleware' => 'web',
	'prefix'     => __('translate.routes.agency'),
	'namespace'  => 'Modules\Agency\Http\Controllers',
], function () {

	Route::get('/', [
		'uses' => 'AgencyController@index',
		'as'   => 'agency.agencycontroller.index',
	]);

	Route::get('/{agency}/брокери', [
		'uses'       => 'AgencyController@brokers',
		'middleware' => 'auth',
	]);
});


Route::group([
	'middleware' => 'web',
	'prefix'     => __('translate.routes.brokers'),
	'namespace'  => 'Modules\Agency\Http\Controllers',
], function () {

	Route::get('/', [
		'uses' => 'BrokerController@index',
		'as'   => 'agency.brokercontroller.index',
	]);
});