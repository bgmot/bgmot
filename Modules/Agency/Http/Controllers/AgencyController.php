<?php

namespace Modules\Agency\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Agency\Entities\Agency;
use View;

class AgencyController extends Controller {

	/**
	 * Display a listing of the resource.
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\View
	 */
	public function index (Request $request) {
		return View::make('Agency::agency.index', [
			'agencies' => Agency::all(),
		]);
	}
}
