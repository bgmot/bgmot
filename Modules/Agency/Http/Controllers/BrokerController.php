<?php

namespace Modules\Agency\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Agency\Entities\Broker;
use View;

class BrokerController extends Controller {

	/**
	 * Display a listing of the resource.
	 * @param Request $request
	 * @param $agency
	 * @return Response
	 */
	public function index (Request $request) {
		/*if (!$agency) {
			return redirect()->route('agency.index', []);
		}

		$brokers = $agency->brokers();*/

		return View::make('Agency::broker.index', [
			//'brokers' => Broker::all(),
		]);
	}
}
