<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_office', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('uuid', 36);
	        $table->unsignedInteger('agency_id');


	        // if any of the fields are empty use the details from the parent agency
	        $table->string('name')->nullable();

	        $table->string('address')->nullable();
	        $table->string('phone')->nullable();
	        $table->string('web')->nullable();

	        $table->unsignedInteger('country_id')->default(1)->nullable();
	        $table->unsignedInteger('area_id')->nullable();
	        $table->unsignedInteger('city_id')->nullable();

	        $table->tinyInteger('is_enabled')->default(1)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_office');
    }
}
