<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	// These will be public details for each agency
        Schema::create('agency_detail', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('uuid', 36);
	        $table->unsignedInteger('agency_id');

	        $table->string('name')->nullable();

	        $table->unsignedInteger('country_id')->default(1);
	        $table->unsignedInteger('area_id');
	        $table->unsignedInteger('city_id');

	        $table->string('address')->nullable();
	        $table->string('phone')->nullable();
	        $table->string('web')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_detail');
    }
}
