<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up () {
		Schema::create('agency', function (Blueprint $table) {
			$table->increments('id');
			$table->string('uuid', 36);
			$table->string('name');

			$table->unsignedInteger('id_number'); // БУЛСТАТ
			$table->unsignedInteger('tax_number'); // Данъчен номер
			$table->string('responsible_person')
			      ->nullable(); // МОЛ, if null use the same data as the firstname/lastname from the register form.
			$table->string('contact_person')
			      ->nullable(); // МОЛ, if null use the same data as the firstname/lastname from the register form.

			$table->string('address')
			      ->nullable();
			$table->string('phone')
			      ->nullable();

			$table->unsignedInteger('country_id')
			      ->default(1);
			$table->unsignedInteger('area_id');
			$table->unsignedInteger('city_id');

			// This will be the user that registered the agency.
			$table->unsignedInteger('main_user_id')
			      ->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down () {
		Schema::dropIfExists('agency');
	}
}
