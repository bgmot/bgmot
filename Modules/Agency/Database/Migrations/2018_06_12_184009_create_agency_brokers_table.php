<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyBrokersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up () {
		Schema::create('agency_brokers', function (Blueprint $table) {
			$table->increments('id');
			$table->string('uuid', 36);

			$table->unsignedInteger('agency_id');
			$table->unsignedInteger('user_id');
			$table->tinyInteger('is_enabled')->default(1)->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down () {
		Schema::dropIfExists('agency_brokers');
	}
}
