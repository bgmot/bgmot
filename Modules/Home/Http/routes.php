<?php

Route::group([
	'middleware' => 'web',
	//'prefix'     => '',
	'namespace'  => 'Modules\Home\Http\Controllers',
], function () {
	Route::get('/', 'HomeController@index');
});
