<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up () {
		Schema::create('area', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('country_id')->default(1);

			$table->string('name');
			$table->longText('country_map')->nullable(); // the <path> element in the country map
			$table->longText('map')->nullable(); // the <path> element when an area is selected.

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down () {
		Schema::dropIfExists('area');
	}
}
