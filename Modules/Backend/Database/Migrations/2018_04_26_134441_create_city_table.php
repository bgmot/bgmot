<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up () {
		Schema::create('city', function (Blueprint $table) {
			$table->increments('id');
			$table->string('uuid', 36);

			$table->unsignedInteger('area_id');
			$table->string('name');
			$table->longText('country_map')->nullable(); // the
			$table->longText('area_map')->nullable();
			$table->tinyInteger('is_area_main_city')->default(0)->nullable();
			$table->tinyInteger('has_neighbors')->default(0)->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down () {
		Schema::dropIfExists('city');
	}
}
