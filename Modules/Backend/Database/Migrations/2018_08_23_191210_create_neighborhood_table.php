<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeighborhoodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up () {
		Schema::create('neighborhood', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('city_id');

			$table->string('name');
			$table->longText('map')
			      ->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down () {
		Schema::dropIfExists('neighborhood');
	}
}
