<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Neighborhood extends Model {

	use SoftDeletes;
	protected $table = 'neighborhood';
	protected $guarded = [];

	public function city () {
		return $this->belongsTo(City::class);
	}
}
