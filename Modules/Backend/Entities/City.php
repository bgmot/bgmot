<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model {

	use SoftDeletes;
	protected $table = 'city';
	protected $guarded = [];

	public function state () {
		return $this->belongsTo(State::class);
	}

	public function neighborhood () {
		return $this->hasMany(Neighborhood::class);
	}
}
