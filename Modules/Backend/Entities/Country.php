<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model {

	use SoftDeletes;
	protected $table = 'country';
	protected $guarded = [];

	public function area () {
		return $this->hasMany(Area::class);
	}

	public function city () {
		return $this->hasManyThrough(Area::class, City::class);
	}
}
