<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model {

	use SoftDeletes;
	protected $table = 'area';
	protected $guarded = [];

	public function city () {
		return $this->hasMany(City::class);
	}

	public function country () {
		return $this->belongsTo(Country::class);
	}
}
