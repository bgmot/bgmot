<?php

namespace Modules\Ad\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Ad\Entities\AdsType;
use Modules\Ad\Entities\Attributes;
use Modules\Ad\Entities\AttributeType;

class AdDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run () {
		Model::unguard();

		// $this->call("OthersTableSeeder");

		$attributes = [
			'heating'            => [
				'Тец',
				'Електриество',
				'Локално отопление',
			],
			'propertiy_settings' => [
				'Обзаведен',
				'Необзаведен',
				'Паркомясто',
				'Гараж',
				'Асансьор',
				'Портиер',
				'Панел',
				'Тухла',
				'Саниран',
				'ЕПК',
				'За ремонт',
				'Комплекс',
				'Окабеляване',
				'Интернет',
				'Климатик',
				'Екстри',
				'СОТ',
				'Kонтрол',
			],
			'propertiy_type'     => [
				'Стая',
				'Едностаен',
				'Двустаен',
				'Тристаен',
				'Четиристаен',
				'Многостаен',
				'Мезонет',
				'Ателие',
				'Гараж',
				'Офис',
				'Магазин',
				'Заведение',
				'Склад',
				'Промишлен имот',
				'Къща',
				'Вила',
				'Парцел',
				'Земеделска земя',
			],
			'currency'           => [
				'EUR',
				'Лева',
			],
			'floor'              => [
				'Етаж',
			],
			'area'               => [
				'Квадратура',
			],
		];

		$adTypes = [
			'Наем',
			'Продажба',
			'Замяна',
			'Съквартирант',
			'В строеж',
		];

		foreach ($adTypes as $adType) {
			AdsType::create([
				'name'        => $adType,
				'description' => $adType,
				'is_enabled'  => 1,
			]);
		}

		foreach ($attributes as $attributeType => $atts) {
			$attrType = AttributeType::create([
				'name'        => $attributeType,
				'description' => $attributeType . ' type ',
			]);

			foreach ($atts as $att) {
				$isRequired = in_array($attributeType, ['currency', 'area', 'floor']) ? 1 : 0;
				$type = ($attributeType == 'floor') ? 'select' : 'checkbox';
				$value = ($attributeType == 'floor') ? json_encode(array_combine(range(0, 30), range(0, 30))) : null;

				$attrType->attribute()
				         ->create([
					         'type'           => $type,
					         //'attribute_type' => $attributeType,
					         'name'           => $att,
					         'value'          => $value,
					         'is_required'    => $isRequired,
				         ]);
			}
		}
	}
}
