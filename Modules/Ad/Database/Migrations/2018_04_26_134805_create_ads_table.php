<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up () {
		Schema::create('ads', function (Blueprint $table) {
			$table->increments('id');
			$table->string('uuid', 36);

			$table->unsignedInteger('user_id');
			$table->tinyInteger('user_sub_id')
			      ->nullable()
			      ->default(0); // Instead of having separate table for property searches or offers (poffers in old platform) we keep here but this flag should be 1 or 2 (1 - search, 2- offer). If its 0 its not created
			$table->string('name')->nullable();
			$table->text('description')
			      ->nullable();
			$table->string('main_image')
			      ->default()
			      ->nullable();
			$table->unsignedInteger('ad_type_id'); // $adTypes (Наем, продажба, замяна)
			$table->tinyInteger('is_initial')
			      ->nullable()
			      ->default(0); // this should indicate that the ad was created during user creation
			$table->unsignedInteger('country_id')->default(1);
			$table->unsignedInteger('city_id');
			$table->unsignedInteger('area_id');
			$table->unsignedInteger('neighbor_id');

			$table->decimal('old_price', 12, 2);
			$table->decimal('price', 12, 2);

			$table->tinyInteger('is_enabled')
			      ->default(1)
			      ->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down () {
		Schema::dropIfExists('ads');
	}
}
