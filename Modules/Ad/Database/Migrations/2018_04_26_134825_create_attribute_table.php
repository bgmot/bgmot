<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute', function (Blueprint $table) {
            $table->increments('id');

            //$table->string('attribute_type');
	        $table->unsignedInteger('attribute_type_id'); // This is how we group the attributes for searching/adding new ads and so on.
	        $table->string('type')->default('checkbox')->nullable(); // This will for generating the search form.
            $table->string('name'); //
	        $table->string('label')->nullble();
	        $table->string('class')->nullble();
            $table->string('value')->nullable(); // For everything other than select we should have this box empty. For select should be a JSON of values.
            $table->integer('sort')->nullable(); // The order of the attributes
            $table->tinyInteger('is_enabled')->default(1)->nullable();

            // Required will be :
	        // Price, Square Meter, Year, Floor ... Should be configurable in Admin
	        // The rest should be optional.
            $table->tinyInteger('is_required')->default(0)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute');
    }
}
