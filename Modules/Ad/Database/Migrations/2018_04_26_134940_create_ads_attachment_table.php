<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsAttachmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_attachment', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('ad_id');
            $table->string('filename'); // this is the original filename
            $table->string('path');

            $table->string('mime');

            $table->tinyInteger('is_enabled')->default(1)->nullable();

            $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_attachment');
    }
}
