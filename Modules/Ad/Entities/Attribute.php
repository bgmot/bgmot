<?php

namespace Modules\Ad\Entities;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model {

	protected $table = 'attribute';
	protected $guarded = [];

	public function adsAttribute () {
		return $this->hasMany(AdsAttribute::class);
	}

	public function type () {
		return $this->belongsTo(AttributeType::class);
	}
}
