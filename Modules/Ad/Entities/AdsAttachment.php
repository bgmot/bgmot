<?php

namespace Modules\Ad\Entities;

use Illuminate\Database\Eloquent\Model;

class AdsAttachment extends Model {

	protected $table = 'ads_attachment';
	protected $guarded = [];

	public function ad () {
		return $this->belongsTo(Ads::class);
	}
}
