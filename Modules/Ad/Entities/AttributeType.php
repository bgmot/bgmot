<?php

namespace Modules\Ad\Entities;

use Illuminate\Database\Eloquent\Model;

class AttributeType extends Model {

	protected $table = 'attribute_type';
	protected $guarded = [];

	public function attribute () {
		return $this->hasMany(Attribute::class);
	}
}
