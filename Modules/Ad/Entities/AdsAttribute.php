<?php

namespace Modules\Ad\Entities;

use Illuminate\Database\Eloquent\Model;

class AdsAttribute extends Model {

	protected $table = 'ads_attribute';
	protected $guarded = [];

	public function ad () {
		return $this->belongsTo(Ads::class);
	}

	public function attribute () {
		return $this->belongsTo(Attribute::class);
	}
}
