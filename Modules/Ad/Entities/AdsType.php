<?php

namespace Modules\Ad\Entities;

use Illuminate\Database\Eloquent\Model;

class AdsType extends Model {

	protected $table = 'ads_type';
	protected $guarded = [];

	public function ad () {
		return $this->hasMany(Ads::class, 'id', 'ad_type_id');
	}
}
