<?php

namespace Modules\Ad\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class Ads extends Model {

	protected $table = 'ads';
	protected $guarded = [];

	public function attributes () {
		return $this->hasMany(AdsAttribute::class, 'ad_id', 'id');
	}

	public function type () {
		return $this->belongsTo(AdsType::class, 'ad_type_id', 'id');
	}

	public function attachments () {
		return $this->hasMany(AdsAttachment::class);
	}

	public function user () {
		return $this->belongsTo(User::class);
	}
}
