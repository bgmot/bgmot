<?php

Route::group(['middleware' => 'web', 'prefix' => 'ad', 'namespace' => 'Modules\Ad\Http\Controllers'], function()
{
    Route::get('/', 'AdController@index');
});
