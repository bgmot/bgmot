<?php

namespace Modules\Ad\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Ad\Entities\Ads;

class AdsRequest extends FormRequest {

	public $currentRequest;

	public function __construct () {
		$this->currentRequest = \Route::getCurrentRequest();

		parent::__construct(func_get_args());
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize () {
		$result = false;

		$user = Auth::user();

		$hasModel = $this->route('ads');

		if ($user) {
			$model = false;

			if ($hasModel) {
				if (is_object($hasModel) && $hasModel instanceof Ads) {
					$model = $hasModel;
				} else {
					$model = Ads::where('uuid', $this->route('database'));
				}
			}

			if ($model) {
				$result = (bool) ($user->routeAccess() && $user->modelAccess($model));
			} else {
				$result = (bool) ($user->routeAccess());
			}
		}

		return $result;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules () {
		return [
			'name' => 'required|max:255',
		];
	}

	public function messages () {
		return [
			'name.required' => 'A name is required',
			'name.max'      => 'The field has to be max :max chars long',

		];
	}

	public function withValidator ($validator) {
		if ($validator->fails()) {
			$validator->after(function ($validator) {
				$this->currentRequest->session()
				                     ->flash('ad', $this->get('ad-params'));
			});
		}
	}
}
