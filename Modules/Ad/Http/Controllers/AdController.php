<?php

namespace Modules\Ad\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Ad\Http\Requests\AdsRequest;

class AdController extends Controller {

	/**
	 * Display a listing of the resource.
	 * @return Response
	 */
	public function index (Request $request) {
		$data = [];

		foreach (DB::connection('mysql_live')
		           ->table('cities')
		           ->where('parent', 0)
		           ->get() as $area) {
			//$data[$area->name_bg] = [];

			foreach (DB::connection('mysql_live')
			           ->table('cities')
			           ->where('parent', $area->id)
			           ->get() as $city) {
				$data[$area->name_bg]['cities'][] = $city->name_bg;

				foreach (DB::connection('mysql_live')
				           ->table('cities')
				           ->where('parent', $city->id)
				           ->get() as $neighbor) {

					//$data[$area->name_bg][$city->name_bg][] = $neighbor->name_bg;

				}

			}
		}

		dd($data);

		return view('Ad::ad.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * @return Response
	 */
	public function create (Request $request) {
		return view('Ad::ad.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * @param  Request $request
	 * @return Response
	 */
	public function store (AdsRequest $request) {

	}

	/**
	 * Show the specified resource.
	 * @return Response
	 */
	public function show (Request $request, $ad) {
		return view('Ad::ad.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 * @return Response
	 */
	public function edit (Request $request, $ad) {
		return view('Ad::ad.edit');
	}

	/**
	 * Update the specified resource in storage.
	 * @param  Request $request
	 * @return Response
	 */
	public function update (AdsRequest $request, $ad) {
	}

	/**
	 * Remove the specified resource from storage.
	 * @return Response
	 */
	public function destroy (Request $request, $ad) {
	}
}
