<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFriendsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up () {
		Schema::create('user_friends', function (Blueprint $table) {
			$table->increments('id');

			$table->unsignedInteger('user_id');

			$table->unsignedInteger('friend_id');

			// When was the request accepted
			$table->timestamp('friend_at')->nullable();

			// Who sent the request
			$table->unsignedInteger('sent_by');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down () {
		Schema::dropIfExists('user_friends');
	}
}
