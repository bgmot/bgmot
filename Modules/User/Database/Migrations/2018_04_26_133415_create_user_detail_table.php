<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_detail', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('uuid', 36);
	        $table->unsignedInteger('user_id');

	        $table->string('firstname');
	        $table->string('lastname');
	        $table->string('address')->nullable();
	        $table->unsignedInteger('country_id')->default(1);
	        $table->unsignedInteger('area_id');
	        $table->unsignedInteger('city_id');
	        $table->string('phone')->nullable();
	        $table->string('web')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_detail');
    }
}
