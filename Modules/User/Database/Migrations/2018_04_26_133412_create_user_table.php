<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('uuid', 40);
            $table->string('email')->unique();
	        $table->string('password');
	        $table->rememberToken();

	        $table->unsignedInteger('user_type_id')->default(1)->nullable();

	        $table->tinyInteger('is_active')->default(0)->nullable();
	        $table->string('activation_code')->nullable();
	        $table->timestamp('activated_at')->nullable();

	        $table->tinyInteger('is_blocked')->default(0)->nullable();

	        $table->tinyInteger('has_notification')->default(0)->nullable();
	        $table->tinyInteger('has_message')->default(0)->nullable();
	        $table->tinyInteger('has_request')->default(0)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
