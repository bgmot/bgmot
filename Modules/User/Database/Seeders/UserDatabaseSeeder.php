<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;
use Modules\User\Entities\UserType;

class UserDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run () {
		Model::unguard();

		// $this->call("OthersTableSeeder");
		$userTypes = [
			'administrator',
			'user',
			'broker',
			'agency',
		];

		$createdUserTypes = [];

		foreach ($userTypes as $userType) {
			$uT = UserType::create([
				'name'        => $userType,
				'description' => $userType . ' user',
			]);

			$createdUserTypes[$uT->id] = $uT;
		}

		foreach ($createdUserTypes as $userTypeId => $userType) {
			$user = User::create([
				'uuid'            => getRandomUUID(),
				'email'           => 'borche+' . $userType->name . '@gmail.com',
				'password'        => \Hash::make('123456'),
				'user_type_id'    => $userTypeId,
				'is_active'       => 1,
				'activation_code' => getRandomUUID(),
				'activated_at'    => now()->subDay(),
				'is_blocked'      => 0,
			]);

			$user->detail()
			     ->create([
				     'firstname'  => 'Borche',
				     'lastname'   => 'Bojcheski',
				     'address'    => 'Ne kazuvam 1235',
				     'city_id'    => '1',
				     'state_id'   => '1',
				     'country_id' => '1',
				     'phone'      => '1234567890',
				     'web'        => 'https://google.som',
			     ]);
		}
	}
}
