<?php

namespace Modules\User\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Modules\User\Entities\User;

// Job for sending activation letters to users.

class Activation extends Job implements ShouldQueue {

	use InteractsWithQueue, SerializesModels;

	private $user = null;

	public function __construct (User $user = null) {
		if (is_null($user)) {
			$this->fail(new \Exception('No user.'));
		}

		$this->user = $user;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function handle () {

	}

	// Sending email
	private function _email () {

	}

	public function failed () {
		Log::error('[' . now() . '] ' . $this->__getIdentifier() . ' Job was failed: [' . $this->lead[$this->params['identifier']] . ']');
	}
}