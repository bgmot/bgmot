<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Ad\Entities\Ads;
use Modules\Search\Entities\SavedSearch;

class User extends Authenticatable {

	use Notifiable, SoftDeletes;

	protected $table = 'user';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'uuid', 'email', 'password', 'user_type_id', 'is_active', 'activation_code', 'activated_at', 'is_blocked',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function detail () {
		return $this->hasOne(UserDetail::class);
	}

	public function friends () {
		return $this->hasMany(UserFriends::class);
	}

	public function messages () {
		return $this->hasMany(UserMessages::class);
	}

	public function notifications () {
		return $this->hasMany(UserNotification::class);
	}

	public function preferences () {
		return $this->hasOne(UserPreferences::class);
	}

	public function savedSearches () {
		return $this->hasMany(SavedSearch::class);
	}

	public function ads () {
		return $this->hasMany(Ads::class);
	}

	// Access check

	public function modelAccess ($model = null) {
		return true;

		if (!is_null($model) && is_object($model)) {
			$reflect = new \ReflectionClass($model);

			if ($this->customer->type == 1) {
				// its a reseller

				if ($this->is_main == 1) {
					// TODO:
					// we should allow access only to the customer area module
					if ($reflect->getShortName() == 'User' ||
						stristr($reflect->getName(), 'Modules\Customer\Entities')) {
						return true;
					}
					//return true;
				}

				return true;

				return (bool) ($model->user_id == $this->id);
			} else if ($this->customer->type == 2) {
				// its a owner

				if ($this->is_main == 1) {
					// TODO:
					// we should allow access only to the customer area module
					if ($reflect->getShortName() == 'User' ||
						stristr($reflect->getName(), 'Modules\Customer\Entities')) {
						return true;
					}
				}

				if ($model->customer_id == $this->customer->id) {
					if (isset($model->is_private)) {
						return (bool) ($model->is_private == 0);
					} else {
						return true;
					}
				}
			} else {
				// patch for now
				if ($this->is_main == 1) {
					// TODO:
					// we should allow access only to the customer area module
					if ($reflect->getShortName() == 'User' ||
						stristr($reflect->getName(), 'Modules\Customer\Entities')) {
						return true;
					}
				}

				if ($model->customer_id == $this->customer->id) {
					if (isset($model->is_private)) {
						return (bool) ($model->is_private == 0);
					} else {
						return true;
					}
				}
			}
		}

		return false;
	}

	public function routeAccess ($route = null) {
		return true;

		if ($this->is_main == 1) {
			return true;
		}

		if (is_null($route)) {
			// get the current route and do checking
			$currentRoute = \Route::getCurrentRoute();
			if (!is_null($currentRoute)) {
				$actions = $currentRoute->getAction();
				$route = isset($actions['as']) ? $actions['as'] : null;
			}
		}

		if (!is_null($route)) {
			// do the logic
			$currentRequest = request();

			if (isset($currentRequest->_control) && in_array((string) $route, $currentRequest->_control['routes'])) {
				return true;
			}
		}

		return false;
	}
}


