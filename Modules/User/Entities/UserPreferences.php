<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPreferences extends Model {

	use SoftDeletes;

	protected $table = 'user_preferences';
	protected $guarded = [];

	public function user() {
		return $this->belongsTo(User::class);
	}
}
