<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFriends extends Model {

	use SoftDeletes;

	protected $table = 'user_friends';
	protected $guarded = [];
	protected $dates = [
		'friend_at',
	];

	public function user () {
		return $this->belongsTo(User::class);
	}
}
