<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserMessages extends Model
{
	use SoftDeletes;

	protected $table = 'user_messages';
	protected $guarded = [];

	protected $dates = [
		'read_at',
	];

	public function user () {
		return $this->belongsTo(User::class);
	}
}
