<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
	return view('welcome');
});*/

use Illuminate\Http\Request;

Route::get('/change-lang/{lang?}', function (Request $request, $lang = null) {
	$supportedLangs = ['en', 'bg'];
	$lang = strtolower($lang);

	if (!in_array($lang, $supportedLangs)) {
		$lang = env('DEFAULT_LOCALE', 'bg');
	}

	$request->session()
	        ->put('lang', $lang);

	return redirect()->to(Redirect::back()->getTargetUrl());
});